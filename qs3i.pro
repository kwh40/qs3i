TARGET      = qs3i
TEMPLATE    =  lib
CONFIG      += dll

include(../../Main/Global/Defines.pro)
include($${VEROSIMSourceFolder}/3rdParty/nlohmann_json/json.pri)
include($${VEROSIMSourceFolder}/3rdParty/qamqp/qamqp.pri)

QT += network

DEFINES += QS3I_EXPORT

# Input
SOURCES = \
    src/authenticationManager.cpp \
    src/brokerInterface.cpp \
    src/brokerInterfaceAMQP.cpp \
    src/brokerInterfaceREST.cpp \
    src/brokerMessage.cpp \
    src/clientCredentialFlow.cpp \
    src/clientRepresentation.cpp \
    src/endpoint.cpp \
    src/entry.cpp \
    src/exception.cpp \
    src/query.cpp \
    src/queryresultiterator.cpp \
    src/oAuthProxyFlow.cpp \
    src/policyEntry.cpp \
    src/policyGroup.cpp \
    src/policySubject.cpp \
    src/policyResource.cpp \
    src/role.cpp \
    src/rqlQuery.cpp \
    src/s3i.cpp  \
    src/service.cpp \
    src/thing.cpp \
    src/token.cpp \
    src/utils.cpp

HEADERS = \
    src/authenticationManager.h \
    src/brokerInterface.h \
    src/brokerInterfaceAMQP.h \
    src/brokerInterfaceREST.h \
    src/brokerMessage.h \
    src/clientCredentialFlow.h \
    src/clientRepresentation.h \
    src/endpoint.h \
    src/entry.h \
    src/exception.h \
    src/query.h \
    src/queryresultiterator.h \
    src/oAuthProxyFlow.h \
    src/policyEntry.h \
    src/policyGroup.h \
    src/policySubject.h \
    src/policyResource.h \
    src/role.h \
    src/rqlQuery.h \
    src/qs3i-global.h \
    src/s3i.h  \
    src/service.h \
    src/thing.h \
    src/token.h \
    src/utils.h \
    src/utils.hpp


#VS_DATA_START
VS_MAINTAINER = reitz@mmi.rwth-aachen.de
VS_ACTIVE = Win64VC14,LinuxUbuntu20,Win64VC142,AndroidArm7QtCreator
VS_LICENSE = TBD, das ist von uns
VS_LICENSE_FILES = 
VS_RECEIVER_WHITELIST = 
VS_ATTRIBUTION_TEXT = 
VS_COPY_FILES = 
#VS_DATA_END
