# QS3I

c++/[Qt](https://www.qt.io/) SDK for the [Smart System Service Infrastructure (S3I)](https://www.kwh40.de/wp-content/uploads/2020/04/KWH40-Standpunkt-S3I-v2.0.pdf)

# Build

This library is built using qmake.

1. Download and build the following dependencies manually.
- [`nlohmann_json`](https://github.com/nlohmann/json), a header only json parsing and processing library
- [`qamqp`](https://github.com/janreitz/qamqp) AMQP Client library. Use this fork, as it provides Authentication using Json Web Tokens.
2. Adjust the `INCLUDEPATH` and `LIBS` [variables](https://doc.qt.io/qt-5/qmake-variable-reference.html#libs) in `qs3i.pro` to match your local directory structure.
3. Build qs3i


