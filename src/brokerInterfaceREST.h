#pragma once

#include "qs3i-global.h"
#include "brokerInterface.h"
#include "exception.h"

#include <QObject>
#include <QNetworkReply>
#include <memory>
namespace S3I
{
   class S3IManager;

   namespace Broker
   {
      class QS3I_DECLSPEC RESTInterface : public ActiveInterface
      {
         Q_OBJECT;

      public:
         RESTInterface(S3IManager* s3i);
         //RESTInterface(RESTInterface&& source);
         /// Start Polling the endpoint. Resets the polling timer if called repeatedly
         void startConsuming(const QString& endpoint) override;
         /// Idempotent
         void stopConsuming(const QString& endpoint) override;
         void sendDirectMessage(std::shared_ptr<Message> msg, const QStringList& endpoints) override;
         void publishEvent(const std::shared_ptr<EventMessage>& evt, const QString& topic) override;

      signals:
         void signalPollingFailed(const QString& endpoint, Exception e);

         void signalSendMessageFailed(std::shared_ptr<Message> msg, NetworkException e);
         void signalSendMessageSucceeded(std::shared_ptr<Message> msg);

      private:
         void pollEndpoint(const QString& endpoint) noexcept;

      private:
         QNetworkAccessManager m_qnam;
	 QMap<QString, std::shared_ptr<QTimer>> m_pollingTimerByEndpoint;
	 QMap<QString, bool> m_currentlyPulling;
         const QString m_brokerBaseUrl = "https://broker.s3i.vswf.dev/";
      };
   }
}