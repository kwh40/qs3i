#include "brokerInterfaceREST.h"
#include "utils.h"
#include "exception.h"
#include "s3i.h"
#include <QNetworkRequest>

namespace S3I
{
   namespace Broker
   {
      void RESTInterface::startConsuming(const QString& endpoint)
      {
         // Polling
         const auto timer = std::make_shared<QTimer>();
         m_pollingTimerByEndpoint.insert(endpoint, timer);
         connect(timer.get(), &QTimer::timeout, this, [this, endpoint] {
            pollEndpoint(endpoint);
         });
         // Instant Poll
         pollEndpoint(endpoint);
         // Subsequent Polls
         timer->setSingleShot(false);
         timer->setInterval(5000);
         timer->start();
      }

      void RESTInterface::stopConsuming(const QString& endpoint)
      {
         m_pollingTimerByEndpoint.remove(endpoint);
      }

      void RESTInterface::pollEndpoint(const QString& endpoint) noexcept
      {
         if(m_currentlyPulling[endpoint]) return;
            m_currentlyPulling.insert(endpoint, true);
         QNetworkRequest req;
         try
         {
            m_s3i->setTokenHeader(req);
         }
         catch (Exception e)
         {
            m_currentlyPulling.insert(endpoint, false);
            emit signalPollingFailed(endpoint, e);
            return;
         }
         req.setUrl(m_brokerBaseUrl + endpoint);
         const auto reply = m_qnam.get(req);
         QObject::connect(reply, &QNetworkReply::finished, this, [this, endpoint, reply]() {
            m_currentlyPulling.insert(endpoint, false);
            if (reply->error() != QNetworkReply::NoError)
            {
               emit signalPollingFailed(endpoint, NetworkException(reply));
            }
            else
            {
               const auto responseBody = reply->readAll();
               if (!responseBody.isEmpty())
               {
                  messageReceived(responseBody);
                  pollEndpoint(endpoint);
               }
            }
         });
      }

      RESTInterface::RESTInterface(S3IManager * s3i)
         : ActiveInterface(s3i)
      {
      }

      void RESTInterface::sendDirectMessage(std::shared_ptr<Message> msg, const QStringList& endpoints)
      {
         QNetworkRequest req;
         m_s3i->setTokenHeader(req);
         req.setUrl(m_brokerBaseUrl + endpoints.join(","));
         req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
         const auto reply = m_qnam.post(req, QByteArray::fromStdString(msg->toJson().dump()));
         QObject::connect(reply, &QNetworkReply::finished, this, [this, reply, msg]() {
            if (reply->error() != QNetworkReply::NoError)
            {
               emit signalSendMessageFailed(msg, NetworkException(reply));
            }
            else
            {
               emit signalSendMessageSucceeded(msg);
            }
         });
      }
      void RESTInterface::publishEvent(const std::shared_ptr<EventMessage>&, const QString&)
      {
         // TODO
      }
   }
}

