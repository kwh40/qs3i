#include "utils.h"

#include <QJsonObject>
#include <QEventLoop>
#include <QNetworkReply>

#include <iostream>
#include <sstream>
#include <string>

namespace S3I
{
   namespace Utils {

      bool jsonObjectPathExists(const QJsonObject & responseObj, const QStringList & pathElements)
      {
         QJsonObject currentObj;
         for (const auto pathElement : pathElements)
         {
            if (currentObj.contains(pathElement) && currentObj[pathElement].isObject())
            {
               currentObj = currentObj[pathElement].toObject();
            }
            else
            {
               return false;
            }
         }
         return true;
      }

      bool jsonObjectHasStringFields(const QJsonObject & obj, const QStringList & fields)
      {
         for (const auto field : fields)
         {
            if (!obj.contains(field) || !obj[field].isString())
               return false;
         }
         return true;
      }

      QString appendUrlParam(QString url, const QString& name, const QStringList& args)
      {
         if (args.isEmpty())
            return url;

         if (!url.endsWith("?"))
         {
            url += "&";
         }
         url += name + "=" + args.join(",");
         return url;
      }

      QNetworkReply* waitForReplyAvailable(QNetworkReply* reply)
      {
         QEventLoop loop;
         QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
#if QT_VERSION > QT_VERSION_CHECK(5, 15, 0)
         QObject::connect(reply, &QNetworkReply::errorOccurred, &loop, &QEventLoop::quit);
#else
         QObject::connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), &loop, SLOT(quit()));
#endif
         loop.exec();

         return reply;
      }

      QString x_www_form_urlencode(const QHash<QString, QString>& parameters)
      {
         QString result;
         bool first_iteration = true;
         for (auto iter = parameters.constKeyValueBegin(); iter != parameters.constKeyValueEnd(); ++iter)
         {
            const QString key = (*iter).first;
            const QString value = (*iter).second;
            QString name_value_pair = QString("%1=%2").arg(urlencode(key)).arg(urlencode(value));

            if (first_iteration)
            {
               first_iteration = false;
               result = name_value_pair;
            }
            else
            {
               result.append(QString("&%1").arg(name_value_pair));
            }
         }
          
         return result;
      }

      QString urlencode(const QString& str)
      {
         return QUrl::toPercentEncoding(str);
      }


      QString getFilenameFromPath(const QString& path)
      {
         QVector<QString> items;
         std::string item;
         std::istringstream itemsStream(path.toStdString());
         char delimiter = 47;
         while (std::getline(itemsStream, item, delimiter))
         {
            items.push_back(QString::fromStdString(item));
         }
         return items.last();
      }

   }
}