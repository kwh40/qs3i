#include "token.h"
#include "exception.h"

#include <QJsonParseError>
namespace S3I
{
   Token::Token(const QJsonObject& header, const QJsonObject& payload, const QString& token)
      : m_header(header)
      , m_payload(payload)
      , m_token(token)
   {
      // TODO Check for mandatory fields
   }

   Token::Token()
      : m_header()
      , m_payload()
   {
   }

   Token::Token(const QString &base64)
   {
      const auto tokenSegments = base64.split('.');
      if (tokenSegments.count() != 3)
         throw JWTParseException("base64 string does not consist of 3 '.' separated parts");

      QJsonParseError headerErr;
      const auto headerObj = QJsonDocument::fromJson(QByteArray::fromBase64(tokenSegments[0].toUtf8()), &headerErr);
      if (headerErr.error != QJsonParseError::NoError)
         throw JWTParseException("QJsonParseError when decoding header : " + headerErr.errorString());

      if (!headerObj.isObject())
         throw JWTParseException("Header is not a JSON Object");

      QJsonParseError payloadErr;
      const auto payloadObj =
         QJsonDocument::fromJson(QByteArray::fromBase64(tokenSegments[1].toUtf8()), &payloadErr);
      if (payloadErr.error != QJsonParseError::NoError)
         throw JWTParseException("QJsonParseError when decoding payload : " + headerErr.errorString());

      if (!payloadObj.isObject())
         throw JWTParseException("Payload is not a JSON Object");

      *this = Token(headerObj.object(), payloadObj.object(), base64);
      return;
   }

   QDateTime Token::getExpiration() const noexcept
   {
      if (m_payload.contains("exp") && m_payload["exp"].isDouble())
         return QDateTime::fromSecsSinceEpoch(m_payload["exp"].toDouble());
      if (m_payload.contains("typ") 
         && m_payload["typ"].isString() 
         && m_payload["typ"].toString() == "Offline")
      {
         // In S3I an offline token is valid for 30 days
         if (m_payload.contains("iat") && m_payload["iat"].isDouble())
            return QDateTime::fromSecsSinceEpoch(m_payload["iat"].toDouble()).addDays(30);
      }
      return QDateTime::fromSecsSinceEpoch(0);
   }

   qint64 Token::secsToExpiration() const noexcept
   {
      return getExpiration().toSecsSinceEpoch() - QDateTime::currentDateTime().toSecsSinceEpoch();
   }

   bool Token::isExpired() const noexcept
   {
      return getExpiration() < QDateTime::currentDateTime();
   }

   bool Token::isEmpty() const noexcept
   {
      return m_header.isEmpty() && m_payload.isEmpty() && m_token.isEmpty();
   }

   QJsonValue Token::getClaim(const QString &claim) const
   {
      return m_payload[claim];
   }

   QString Token::encoded() const
   {
      return m_token;
   }

   AccessToken::AccessToken()
   {
   }

   AccessToken::AccessToken(const QString & base64)
      : Token(base64)
   {
   }

   RefreshToken::RefreshToken()
   {
   }

   RefreshToken::RefreshToken(const QString & base64)
      : Token(base64)
   {
   }
}