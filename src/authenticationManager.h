#ifndef AUTHENTICATION_MANAGER_H
#define AUTHENTICATION_MANAGER_H

#include "token.h"
#include "qs3i-global.h"

#include <QObject>
#include<qnetworkaccessmanager.h>

namespace S3I
{
   // TODO wrap this in the 'Auth' namespace
   class QS3I_DECLSPEC AuthenticationManager : public QObject
   {
      Q_OBJECT

   public:
      AuthenticationManager(const QString& clientId, const QString& clientSecret);

      virtual AccessToken getAccessToken(qint64 minimumRemainingLifetimeSeconds) = 0;
      void invalidateSession();
      
      // Set token members to default constructed
      void resetTokens();

      /// We need these methods to persist RefreshTokens
      void setRefreshToken(const RefreshToken& token);
      virtual RefreshToken getRefreshToken() const;
      /// Invalidates existing tokens, if scopes changes
      void setScopes(const QStringList& scopes);
      QStringList getScopes() const;

   signals:
      void signalRefreshTokenUpdated();

   protected:
      QNetworkAccessManager m_qnam;

      AccessToken m_accessToken;
      RefreshToken m_refreshToken;
      const QString m_clientId;
      const QString m_clientSecret;
      QStringList m_scopes;
   };

}
#endif // AUTHENTICATION_MANAGER_H
