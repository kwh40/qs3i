#ifndef SERVICE_H
#define SERVICE_H
#include "qs3i-global.h"
#include "endpoint.h"
namespace S3I
{
   namespace Directory {

      class QS3I_DECLSPEC Service
      {
      public:
         Service();

         QList<Endpoint> getEndpoints();
         QMap<QString, QString> parameterTypes; // Should we specify S3I Types?
         QMap<QString, QString> resultTypes; // Should we specify S3I Types?
         QString serviceType;

      };

   }
}
#endif // SERVICE_H
