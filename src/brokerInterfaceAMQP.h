#pragma once

#include "qs3i-global.h"
#include "brokerInterface.h"
#include "exception.h"

#include <qamqpclient.h>
#include <qmutex.h>

namespace S3I
{
   class S3IManager;

   namespace Broker
   {

      class QS3I_DECLSPEC AMQPInterface : public ActiveInterface
      {
         Q_OBJECT;

      public:

         enum ConnectionState {
            Connecting,
            Connected,
            Disconnecting,
            Disconnected,
            Reconnecting,
         };
         Q_ENUM(ConnectionState)

         AMQPInterface(S3IManager* s3i);
         void startConsuming(const QString& endpoint) override;
         /// Idempotent
         void stopConsuming(const QString& endpoint) override;
         bool isConsuming() const;
         void sendDirectMessage(std::shared_ptr<Message> msg, const QStringList& endpoints) override;
         void publishEvent(const std::shared_ptr<EventMessage>& evt, const QString& topic) override;
         void sendHelloWorld(const QString& routingKey);

         void setAutoReconnect(bool autoReconnect) noexcept;
         bool getAutoReconnect() const noexcept;

         void refreshToken();

         ConnectionState getConnectionState() const;

      signals:
         void signalPollingFailed(const QString& endpoint, Exception e);
         void signalConnectionStateChanged(const ConnectionState& newState);

      private slots:
         void slotClientConnected();
         void slotClientDisconnected();
         void slotAmqpError(QAMQP::Error error);
         void slotSslError(const QList<QSslError>& errors);
         void slotSocketError(QAbstractSocket::SocketError error);
         void slotTokenRefresh();
         void slotMessageReceived();

      private:
         void connectToBroker(
            const QString& brokerUrl = "rabbitmq.s3i.vswf.dev",
            const quint16& port = 5672,
            const QString& vHost = "s3i",
            const quint16& heartbeatDelayInSecs = 0);
         void setClientAuthentication();
         void disconnectFromBroker();
         QString stringFromQamqpError(const QAMQP::Error& error);

         QAmqpClient m_client;
         QAmqpQueue* m_queue = nullptr;
         QAmqpExchange* m_eventExchange = nullptr;
         QAmqpExchange* m_directExchange = nullptr;
         QTimer m_tokenRefreshTimer;
         ConnectionState m_connectionState = Disconnected;
         bool m_autoReconnect; // Is only read
         bool m_isConsuming;
         std::vector<std::function<void(void)>> m_onConnectedCallbacks;
         std::vector<std::function<void(void)>> m_onDisconnectedCallbacks;
         std::vector<std::function<void(void)>> m_onEventExchangeOpenCallbacks;
         std::vector<std::function<void(void)>> m_onDirectExchangeOpenCallbacks;
      };
   }
}