#include "clientCredentialFlow.h"
#include "utils.h"
#include "exception.h"

#include <QJsonDocument>

namespace S3I
{
   
   ClientCredentialFlow::ClientCredentialFlow(const QString& clientId, const QString& clientSecret)
      : AuthenticationManager(clientId, clientSecret)
   {
   }

   AccessToken ClientCredentialFlow::getAccessToken(qint64 minimumRemainingLifetimeSeconds)
   {
      // Current AccessToken valid?
      if (m_accessToken.secsToExpiration() > minimumRemainingLifetimeSeconds)
      {
         return m_accessToken;
      }
      
      // Get Token
      QNetworkRequest getTokenReq;
      getTokenReq.setUrl(QString("https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/token?"));// TODO make configurable

      // Keycloak expects that parameters are passed as urlencoded body
      getTokenReq.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/x-www-form-urlencoded");
      auto getTokenBody = S3I::Utils::appendUrlParam(QString(), "grant_type", { "client_credentials" });
      getTokenBody = S3I::Utils::appendUrlParam(getTokenBody, "client_id", { m_clientId });
      getTokenBody = S3I::Utils::appendUrlParam(getTokenBody, "client_secret", { m_clientSecret });
      //getTokenBody = S3I::Utils::appendUrlParam(getTokenBody, "scope", { m_scopes.join(" ") });

      const auto getTokenReply = Utils::waitForReplyAvailable(m_qnam.post(
         getTokenReq,
         getTokenBody.toUtf8()
      ));

      if (getTokenReply->error() != QNetworkReply::NetworkError::NoError)
      {  
         throw NetworkException(getTokenReply);
      }
      else 
      {
         const auto replyJson = QJsonDocument::fromJson(getTokenReply->readAll());
         if (replyJson.isNull())
            throw S3I::InvalidJsonException(QString(""));

         const auto token = parseTokenFromJsonResponse(replyJson);
         m_accessToken = token;
         return m_accessToken;
      }
   }

   AccessToken ClientCredentialFlow::parseTokenFromJsonResponse(const QJsonDocument & jsonResponse) const
   {
      AccessToken token;

      if (!jsonResponse.isObject())
      {
         throw InvalidJsonException("ClientCredentialFlow::parseTokenFromJsonResponse -> Response body was not a json object");
      }
      const auto replyObject = jsonResponse.object();

      if (!replyObject.contains("access_token"))
      {
         throw SchemaException("ClientCredentialFlow::parseTokenFromJsonResponse -> Response does not contain field 'access_token'");
      }

      QString accessTokenString = replyObject["access_token"].toString();
      if (accessTokenString.isNull() || accessTokenString.isEmpty())
      {
         throw SchemaException("ClientCredentialFlow::parseTokenFromJsonResponse -> Field 'access_token' empty or not of type string");
      }

      return AccessToken(accessTokenString);
   }
}