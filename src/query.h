#ifndef QUERY_H
#define QUERY_H

#include "qs3i-global.h"
#include "rqlQuery.h"

#include <QStringList>

#include <memory>

namespace S3I
{
   class S3IManager;
   namespace Directory {
      class Thing;
   }


   class QS3I_DECLSPEC Query
   {
   public:
      static Query create();

      Query& filter(std::unique_ptr<RQL::RQLQuery> rqlQuery);
      /// Field 'thingId' is mandatory and will be appended on build() if missing
      Query& fields(const QStringList& fields);
      Query& namespaces(const QStringList& namespaces);
      // Separate functions for different options might be nicer
      Query& options(const QStringList& option);

      QList<Directory::Thing> execute(S3IManager* s3i);

   private:
      Query();

      QString build() const;

      QString m_filter;
      QStringList m_fields;
      QStringList m_namespaces;
      QStringList m_options;
   };
}
#endif // QUERY_H
