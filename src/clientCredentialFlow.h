#pragma once

#include "authenticationManager.h"

namespace S3I
{

   class QS3I_DECLSPEC ClientCredentialFlow : public AuthenticationManager
   {
      Q_OBJECT

   public:
      ClientCredentialFlow(const QString& clientId, const QString& clientSecret);
      AccessToken getAccessToken(qint64 minimumRemainingLifetimeSeconds) override;

   private:
      AccessToken parseTokenFromJsonResponse(const QJsonDocument& jsonResponse) const;

   };

}