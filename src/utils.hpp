template<typename T> QJsonArray Utils::jsonArrayFromIterable(T iterable)
{
   QJsonArray arr;

   for (const auto elem : iterable)
   {
      arr.append(elem);
   }
   return arr;
}