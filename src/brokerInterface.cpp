#include "brokerInterface.h"
#include "exception.h"
#include "s3i.h"

namespace S3I
{
   using namespace Broker;

   ActiveInterface::ActiveInterface(S3IManager* s3i)
      : m_s3i(s3i)
   {
   }

   void ActiveInterface::messageReceived(const QString& msg)
   {
      json messageDoc;

      try
      {
         messageDoc = json::parse(msg.toStdString());
      }
      catch (std::exception e)
      {
         // TODO handle encrypted messages
         emit signalInvalidMessageReceived(msg, "Not valid Json");
         return;
      }

      if (messageDoc.is_null())
      {
         emit signalInvalidMessageReceived(msg, "Not valid Json");
         return;
      }

      if (!messageDoc.is_object())
      {
         emit signalInvalidMessageReceived(msg, "Not a Json Object");
         return;
      }

      const auto obj = messageDoc;
      if (!obj.contains("messageType"))
      {
         emit signalInvalidMessageReceived(msg, "Missing 'messageType' field");
         return;
      }

      const auto messageTypeVal = obj["messageType"];
      if (!messageTypeVal.is_string())
      {
         qDebug() << "S3I::Broker::ActiveInterface::messageReceived -> field 'messageType' is not a string";
         emit signalInvalidMessageReceived(msg, "Field 'messageType' is not a string");
         return;
      }

      const auto messageType = QString(messageTypeVal.get<std::string>().c_str());
      try
      {
         if (messageType == "userMessage")
         {
            // TODO -> Checks einbauen falls da was schief geht
            const auto userMsg = UserMessage::fromJson(obj);
            // An wen wird das weitergegeben? Oder ein Signal emitten und andere k�nnen sich connecten?
            emit signalUserMessageReceived(userMsg);
         }
         else if (messageType == "serviceRequest")
         {
            auto serviceRequest = ServiceRequest::fromJson(obj);
            emit signalServiceRequestReceived(serviceRequest);
         }
         else if (messageType == "serviceRequestStatus")
         {
            auto serviceRequestStatus = ServiceRequestStatus::fromJson(obj);
            emit signalServiceRequestStatusReceived(serviceRequestStatus);
         }
         else if (messageType == "serviceReply")
         {
            auto serviceReply = ServiceReply::fromJson(obj);

            emit signalServiceReplyReceived(serviceReply);

         }
         else if (messageType == "getValueRequest")
         {
            auto getValueRequest = GetValueRequest::fromJson(obj);
            emit signalGetValueRequestReceived(getValueRequest);
         }
         else if (messageType == "getValueReply")
         {
            auto getValueReply = GetValueReply::fromJson(obj);

            emit signalGetValueReplyReceived(getValueReply);

         }
         else if (messageType == "setValueRequest")
         {
            auto setValueRequest = SetValueRequest::fromJson(obj);
            emit signalSetValueRequestReceived(setValueRequest);
         }
         else if (messageType == "setValueReply")
         {
            auto setValueReply = SetValueReply::fromJson(obj);

            emit signalSetValueReplyReceived(setValueReply);

         }
         else if (messageType == "createAttributeRequest")
         {
            auto createAttributeRequest = CreateAttributeRequest::fromJson(obj);

            //            handleCreateAttributeRequest(createAttributeRequest);

         }
         else if (messageType == "createAttributeReply")
         {
            auto createAttributeReply = CreateAttributeReply::fromJson(obj);

            emit signalCreateAttributeReplyReceived(createAttributeReply);

         }
         else if (messageType == "deleteAttributeRequest")
         {
            auto deleteAttributeRequest = DeleteAttributeRequest::fromJson(obj);

            //            handleDeleteAttributeRequest(deleteAttributeRequest);

         }
         else if (messageType == "deleteAttributeReply")
         {
            auto deleteAttributeReply = DeleteAttributeReply::fromJson(obj);

            emit signalDeleteAttributeReplyReceived(deleteAttributeReply);

         }
         else if (messageType == "customEventRequest")
         {
            auto customEventRequest = CustomEventRequest::fromJson(obj);

            emit signalCustomEventRequestReceived(customEventRequest);

         }
         else if (messageType == "customEventReply")
         {
            auto customEventReply = CustomEventReply::fromJson(obj);

            emit signalCustomEventReplyReceived(customEventReply);

         }
      }
      catch (Exception e) {
         emit signalInvalidMessageReceived(msg, e.what());
      }

   }

}