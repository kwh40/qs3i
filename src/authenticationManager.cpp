#include "authenticationManager.h"
#include "exception.h"
#include "utils.h"

namespace S3I
{
   AuthenticationManager::AuthenticationManager(
      const QString& clientId,
      const QString& clientSecret
   )
      : m_clientId(clientId)
      , m_clientSecret(clientSecret)
      , m_qnam()
   {
   }

   void AuthenticationManager::invalidateSession()
   {
      QNetworkRequest req(QUrl("https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/logout")); // TODO make configurable
      req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
      QHash<QString, QString> params;
      params["client_id"] = m_clientId;
      params["client_secret"] = m_clientSecret;
      params["refresh_token"] = m_refreshToken.encoded();
      const QString body = Utils::x_www_form_urlencode(params);

      const auto refreshReply = Utils::waitForReplyAvailable(m_qnam.post(req, body.toUtf8()));

      // This is the expected behavior. 
      // Status Code 204 can also be interpreted as: Request successfully processed, but returned no content
      if (refreshReply->error() != QNetworkReply::NetworkError::AuthenticationRequiredError
         // This would be unexpected, but I don't want to throw on 200
         && refreshReply->error() != QNetworkReply::NetworkError::NoError)
      {
         throw NetworkException(refreshReply);
      }
   }

   void AuthenticationManager::setRefreshToken(const RefreshToken & token)
   {
      m_refreshToken = token;
   }

   RefreshToken AuthenticationManager::getRefreshToken() const
   {
      return m_refreshToken;
   }

   void AuthenticationManager::setScopes(const QStringList & scopes)
   {
      // equality operator on QStringList considers order
      // We don't care about order
      for (const auto s : scopes)
      {
         if (!m_scopes.contains(s))
         {
            resetTokens();
            emit signalRefreshTokenUpdated();
         }
      }

      m_scopes = scopes;
   }

   QStringList AuthenticationManager::getScopes() const
   {
      return m_scopes;
   }

   void AuthenticationManager::resetTokens()
   {
      m_accessToken = AccessToken();
      m_refreshToken = RefreshToken();
   }
}