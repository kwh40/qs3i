#include "query.h"
#include "utils.h"
#include "s3i.h"
#include "thing.h"

#include <QJsonDocument>
#include <QJsonObject>
namespace S3I
{
   Query::Query()
   {
   }


   Query Query::create()
   {
      return Query();
   }

   Query& Query::filter(std::unique_ptr<RQL::RQLQuery> rqlQuery)
   {
      m_filter = rqlQuery->to_string();
      return *this;
   }

   Query& Query::fields(const QStringList& fields)
   {
      m_fields = fields;
      return *this;
   }

   Query& Query::namespaces(const QStringList& namespaces)
   {
      m_namespaces = namespaces;
      return *this;
   }

   Query& Query::options(const QStringList& option)
   {
      m_options = option;
      return *this;
   }

   QString Query::build() const
   {
      QString url = "search/things?";

      if (!m_filter.isEmpty())
      {
         url = Utils::appendUrlParam(url, "filter", { m_filter });
      }
      if (!m_fields.isEmpty())
      {
         // fields need to contain "thingId" to receive a parsable thing
         auto fields = m_fields;
         if (!fields.contains("thingId"))
            fields.append("thingId");
         url = Utils::appendUrlParam(url, "fields", { fields });
      }
      if (!m_namespaces.isEmpty())
      {
         url = Utils::appendUrlParam(url, "namespaces", { m_namespaces });
      }
      if (!m_options.isEmpty())
      {
         url = Utils::appendUrlParam(url, "option", { m_options });
      }

      return url;
   }

   QList<Directory::Thing> Query::execute(S3IManager* s3i)
   {
      return s3i->executeQuery(build());
   }

}