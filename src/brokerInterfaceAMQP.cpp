#include "brokerInterfaceAMQP.h"
#include "utils.h"
#include "exception.h"
#include "s3i.h"

#include <qamqpqueue.h>
#include <qamqpexchange.h>
#include <qamqpauthenticator.h>

namespace S3I
{
   namespace Broker
   {

#define TOKEN_REFRESH_BEFORE_EXPIRE (4 * 60)

      AMQPInterface::AMQPInterface(S3IManager* s3i)
         : ActiveInterface(s3i)
         , m_isConsuming(false)
         , m_autoReconnect(false)
      {
         connect(&m_client, &QAmqpClient::connected, this, &AMQPInterface::slotClientConnected);
         connect(&m_client, &QAmqpClient::disconnected, this, &AMQPInterface::slotClientDisconnected);
         connect(&m_client, QOverload<QAMQP::Error>::of(&QAmqpClient::error), this, &AMQPInterface::slotAmqpError);
         connect(&m_client, QOverload<QAbstractSocket::SocketError>::of(&QAmqpClient::socketError), this, &AMQPInterface::slotSocketError);
         connect(&m_client, &QAmqpClient::sslErrors, this, &AMQPInterface::slotSslError);
      }

      void AMQPInterface::startConsuming(const QString& endpoint)
      {
         auto startConsumingLambda = [this, endpoint]() {

            auto onChannelOpenedCallback = [this]() {
               if (m_queue->consume())
               {
                  qDebug() << "AMQPInterface::startConsuming -> waiting for messages to queue " + m_queue->name();
                  m_isConsuming = true;
               }
               else
               {
                  qDebug() << "AMQPInterface::startConsuming -> m_queue.consume() failed";
               }
            };
            // It seems to be best-practice to declare queues before using them.
            // I talked to Jiahang about this and he decided to not do this in our case,
            // because queues are managed by something else (S3I) and there are permission issues
            if (m_queue && m_queue->isOpen())
            {
               onChannelOpenedCallback();
            }
            else
            {
               // TODO remember which endpoints are being consumed, to restart this after reconnect
               // Queues are owned by qamqp via the QObject parenting mechanism
               // Queues are cleared on the next connection
               m_queue = m_client.createQueue(endpoint);
               QObject::connect(m_queue, &QAmqpQueue::opened, onChannelOpenedCallback);
               QObject::connect(m_queue, &QObject::destroyed, [this] {
                  qDebug() << "Queue destroyed. m_queue:" << m_queue;
                  m_queue = nullptr;
                  m_isConsuming = false;
                  });
               QObject::connect(m_queue, &QAmqpQueue::closed, [this]() {
                  m_isConsuming = false;
                  });
               connect(m_queue, &QAmqpQueue::messageReceived,this, &AMQPInterface::slotMessageReceived);
            }
         };

         if (m_client.isConnected())
         {
            startConsumingLambda();
         }
         else
         {
            m_onConnectedCallbacks.emplace_back(startConsumingLambda);
            connectToBroker();
         }
      }

      void AMQPInterface::slotMessageReceived()
      {
         if (!m_queue)
            return;

         QAmqpMessage message = m_queue->dequeue();
         m_queue->ack(message);

         qDebug() << "AMQPInterface::messageReceived -> " << message.payload();

         messageReceived(message.payload());
      }

      void AMQPInterface::stopConsuming(const QString& endpoint)
      {
         disconnectFromBroker(); // TODO is this the right interpretation, or should I just stop consuming from the queue?

         //if (m_queue && m_queue->isConsuming())
         //   m_queue->cancel();
      }

      bool AMQPInterface::isConsuming() const
      {
         return m_isConsuming;
      }

      void AMQPInterface::sendDirectMessage(std::shared_ptr<Message> msg, const QStringList& endpoints)
      {
         const auto serializedMessage = msg->toQByteArray();

         auto sendDirectMessageLambda = [this, serializedMessage, endpoints]() {
            for (const auto endpoint : endpoints)
            {
               m_directExchange->publish(serializedMessage, endpoint);
            }
         };

         if (m_directExchange && m_directExchange->isOpen())
         {
            sendDirectMessageLambda();
         }
         else
         {
            m_onConnectedCallbacks.emplace_back(sendDirectMessageLambda);
            connectToBroker();
         }
      }

      void AMQPInterface::publishEvent(const std::shared_ptr<EventMessage>& evt, const QString& topic)
      {
         auto publishEventLambda = [this, evt, topic] {
            // Exchanges are owned by qamqp via the QObject aprenting mechanism.
            // When creating an exchange with an existing name,
            // qamqp will return a pointer to the existing exchange instead of creating a new one.
            m_eventExchange->publish(evt->toQByteArray(), topic);
         };

         if (m_eventExchange && m_eventExchange->isOpen())
         {
            publishEventLambda();
         }
         else
         {
            m_onEventExchangeOpenCallbacks.emplace_back(publishEventLambda);
            connectToBroker();
         }
      }

      void AMQPInterface::setAutoReconnect(bool autoReconnect) noexcept
      {
         m_autoReconnect = autoReconnect;
      }

      bool AMQPInterface::getAutoReconnect() const noexcept
      {
         return m_autoReconnect;
      }

      void AMQPInterface::sendHelloWorld(const QString& routingKey)
      {
         auto helloWorldMsg = std::make_shared<UserMessage>();
         helloWorldMsg->m_text = "Hello World!";

         return sendDirectMessage(helloWorldMsg, { routingKey } );
      }

      void AMQPInterface::setClientAuthentication()
      {
         const auto token = m_s3i->getAuthManager()->getAccessToken(5);
         auto JWTAuth = new QAmqpJWTAuthenticator(token.encoded());
         // QAmqpClient takes ownership of the Authenticator object
         m_client.setAuth(JWTAuth);


         // This is to avoid multiple connections of signal to slot and the related debug output
         static bool connected = false;
         if (!connected)
         {
            connected = connect(&m_tokenRefreshTimer, SIGNAL(timeout()), this, SLOT(slotTokenRefresh()));
         }

         // If singleShot is true, the timer will be activated only once.
         m_tokenRefreshTimer.setSingleShot(true);
         // If the timer is already running, it will be stopped and restarted.
         m_tokenRefreshTimer.start((token.secsToExpiration() - TOKEN_REFRESH_BEFORE_EXPIRE) * 1000);
      }

      void AMQPInterface::refreshToken()
      {
         setClientAuthentication();

         m_connectionState = Reconnecting;
         emit signalConnectionStateChanged(m_connectionState);
         disconnectFromBroker();
      }

      AMQPInterface::ConnectionState AMQPInterface::getConnectionState() const
      {
         return m_connectionState;
      }

      void AMQPInterface::slotTokenRefresh()
      {
         refreshToken();
      }

      void AMQPInterface::connectToBroker(
         const QString& brokerUrl,
         const quint16& port,
         const QString& vHost,
         const quint16& heartbeatDelayInSecs)
      {
         if (m_connectionState == Connecting || m_connectionState == Connected)
            return;

         m_client.setHost(brokerUrl);
         m_client.setPort(port);
         m_client.setVirtualHost(vHost);
         m_client.setHeartbeatDelay(heartbeatDelayInSecs);
         m_client.setAutoReconnect(false); // We implement a custom autoReconnect to include token refresh

         setClientAuthentication();

         qDebug() << "AMQPInterface::connectToBroker -> Connecting to " + m_client.host() + ":" + QString::number(m_client.port());

         // createExchange calls QAmqpExchange::open() after client is connected
         m_eventExchange = m_client.createExchange("eventExchange");
         connect(m_eventExchange, &QAmqpChannel::opened, this, [this] {
            for (auto callback : m_onEventExchangeOpenCallbacks)
            {
               callback();
            }
            m_onEventExchangeOpenCallbacks.clear();
         });

         // createExchange calls QAmqpExchange::open() after client is connected
         m_directExchange = m_client.createExchange("demo.direct");
         connect(m_directExchange, &QAmqpChannel::opened, this, [this] {
            for (auto callback : m_onDirectExchangeOpenCallbacks)
            {
               callback();
            }
            m_onDirectExchangeOpenCallbacks.clear();
         });

         m_connectionState = Connecting;
         emit signalConnectionStateChanged(m_connectionState);
         m_client.connectToHost();
      }

      void AMQPInterface::slotClientConnected()
      {
         m_connectionState = Connected;
         emit signalConnectionStateChanged(m_connectionState);

         qDebug() << "AMQPInterface::slotClientConnected";

         for (auto callback : m_onConnectedCallbacks)
         {
            callback();
         }
         m_onConnectedCallbacks.clear();
      }

      void AMQPInterface::disconnectFromBroker()
      {
         if (m_connectionState == Connected)
         {
            m_connectionState = Disconnecting;
            emit signalConnectionStateChanged(m_connectionState);
         }

         m_client.disconnectFromHost();
      }

      void AMQPInterface::slotClientDisconnected()
      {
         qDebug() << "AMQPInterface::slotClientDisconnected";

         // Intentional disconnect
         switch (m_connectionState)
         {
         case Disconnecting:
            m_connectionState = Disconnected;
            emit signalConnectionStateChanged(m_connectionState);
            break;
         case Reconnecting:
            connectToBroker();
            break;
         case Connected:
            if (getAutoReconnect())
            {
               connectToBroker();
            }
            else
            {
               m_connectionState = Disconnected;
               emit signalConnectionStateChanged(m_connectionState);
            }
            break;
         default:
            qDebug() << "Unhandled ConnectionState: " << m_connectionState;
            break;
         }

      }

      void AMQPInterface::slotAmqpError(QAMQP::Error error)
      {
         qDebug() << "AMQPInterface::slotAmqpError -> " << stringFromQamqpError(error);
      }

      void AMQPInterface::slotSslError(const QList<QSslError>& errors)
      {
         QString errorStrings;
         for (auto error : errors)
         {
            errorStrings += error.errorString() + "; ";
         }
         qDebug() << "AMQPInterface.QamqpClient::sslErrors -> " + errorStrings;
      }

      void AMQPInterface::slotSocketError(QAbstractSocket::SocketError error)
      {
         // SocketError occurs in two circumstances:
         // 1. Error during Connection (Connected):
         // - slotClientdisconnected
         //    a) (with autoReconnect = true) -> connectToBroker -> (changes state to Connecting)
         //    b) (with autoReconnect = false) -> changes state to Disconnected
         // - slotSocketError
         //
         // 2. Error while establishing a Connection (Connecting)
         // - slotSocketError

         qDebug() << "AMQPInterface.QamqpClient::slotSocketError socketError: " << error << "\tConnectionState: " << m_connectionState;

         switch (m_connectionState) {
         case Disconnected:
            // 1b
            // do nothing, slotClientDisconnected already decided not to call connectToBroker
            break;
         case Connecting:
            // 1a
            // do nothing, slotClientDisconnected already called connectToBroker
            if (m_client.socketState() == QAbstractSocket::SocketState::ConnectingState
               || m_client.socketState() == QAbstractSocket::SocketState::HostLookupState)
            {}
            // 2
            // attempt reconnect
            else
            {
               m_connectionState = Disconnected;
               emit signalConnectionStateChanged(m_connectionState);
               // I need to prevent infinite recursion with connectToBroker, because slotSocketError is called directly.
               QTimer::singleShot(100, [this] { // msec
                  connectToBroker();
                  });
            }
            break;
         default:
            qDebug() << "AMQPInterface.QamqpClient::socketError unhandled ConnectionState -> " << m_connectionState;
         }
      }

      QString AMQPInterface::stringFromQamqpError(const QAMQP::Error& error)
      {
         if (error == QAMQP::Error::NoError)
         {
            return QString("NoError");
         }
         if (error == QAMQP::Error::ContentTooLargeError)
         {
            return QString("ContentTooLargeError");
         }
         if (error == QAMQP::Error::NoRouteError)
         {
            return QString("NoRouteError");
         }
         if (error == QAMQP::Error::NoConsumersError)
         {
            return QString("NoConsumersError");
         }
         if (error == QAMQP::Error::ConnectionForcedError)
         {
            return QString("ConnectionForcedError");
         }
         if (error == QAMQP::Error::InvalidPathError)
         {
            return QString("InvalidPathError");
         }
         if (error == QAMQP::Error::AccessRefusedError)
         {
            return QString("AccessRefusedError");
         }
         if (error == QAMQP::Error::NotFoundError)
         {
            return QString("NotFoundError");
         }
         if (error == QAMQP::Error::ResourceLockedError)
         {
            return QString("ResourceLockedError");
         }
         if (error == QAMQP::Error::PreconditionFailedError)
         {
            return QString("PreconditionFailedError");
         }
         if (error == QAMQP::Error::FrameError)
         {
            return QString("FrameError");
         }
         if (error == QAMQP::Error::SyntaxError)
         {
            return QString("SyntaxError");
         }
         if (error == QAMQP::Error::CommandInvalidError)
         {
            return QString("CommandInvalidError");
         }
         if (error == QAMQP::Error::ChannelError)
         {
            return QString("ChannelError");
         }
         if (error == QAMQP::Error::UnexpectedFrameError)
         {
            return QString("UnexpectedFrameError");
         }
         if (error == QAMQP::Error::ResourceError)
         {
            return QString("ResourceError");
         }
         if (error == QAMQP::Error::NotAllowedError)
         {
            return QString("NotAllowedError");
         }
         if (error == QAMQP::Error::NotImplementedError)
         {
            return QString("NotImplementedError");
         }
         if (error == QAMQP::Error::InternalError)
         {
            return QString("NotImplementedError");
         }
         else
         {
            return QString("UnknownError");
         }
      }

}
}

