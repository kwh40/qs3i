#pragma once

#include "qs3i-global.h"

#include <QString>
#include <memory>

// RQL is a query language that can be transported in the URI of a Request
// An RQL Query is composes of relational and logical operators.
// relational operators compare properties, specified using the json pointer notation,
// and values

// I'll try to represent this as an expression tree, where each element is a QueryElement
//        and
//     /         \
//   lt          or
//   / \	      /     \
// "p1" 10   eq        eq
//  			/   \    /    \
//       "pt1"   5 "pt1"   7

// Example from https://www.eclipse.org/ditto/basic-rql.html
// and(eq(foo, "ditto"), lt(bar, 10))
//          and
//       /        \
//    eq            lt
//   /   \        /    \
// foo  "ditto"  bar   10

// Ditto supports these RQL Operators:
// Relational operators(Query property, Query value)
// - eq
// - ne
// - gt
// - ge
// - lt
// - le
// - in  Has multiple values
// - like
// - exists
// Logical operators(a, b)
// - and
// - or
// - not Has only one value

// Values can be
// <value> = <number>, <string>, true, false, null
// <number> = double, integer
// <string> = ", url-encoded-string, "

using namespace std;
namespace S3I
{
   namespace RQL {
      enum class RelationalOperatorType {
         eq,
         ne,
         gt,
         ge,
         lt,
         le,
         in,
         like,
         exists,
      };

      QString to_string(RelationalOperatorType operatorType);

      enum class LogicalOperatorType {
         AND,
         OR,
         NOT
      };

      QString to_string(LogicalOperatorType operatorType);

      enum class ValueType {
         _BOOL,
         _NULL,
         _INT,
         _DOUBLE,
         _STRING,
      };

      class QS3I_DECLSPEC RQLQuery {
      public:
         // I think there are multiple formats, so this should be varied
         // Also provide option to return percent encoded string
         virtual QString to_string() const = 0;
      };

      class QS3I_DECLSPEC Value : public RQLQuery {
      public:
         Value(const QString& value);
         Value(bool value);
         Value(int value);
         Value(double value);
         Value();
         static Value _NULL();

         QString to_string() const override;

      private:
         ValueType m_valueType;
         // Ich könnte auch einen Variant machen
         QString m_stringValue;
         bool m_boolValue;
         int m_intValue;
         double m_doubleValue;
      };

      class QS3I_DECLSPEC RelationalQuery : public RQLQuery {
      public:
         RelationalQuery(const QString& prop, RelationalOperatorType op, const Value& value);

         QString to_string() const override;
      private:
         QString m_property;	// JSON Pointer to the requested property
         Value m_value;		// Property is compared with this Value
         RelationalOperatorType m_operatorType; // Using this relation
      };
      // Factory functions
      QS3I_DECLSPEC unique_ptr<RQLQuery> eq(const QString& prop, const Value& value);
      QS3I_DECLSPEC unique_ptr<RQLQuery> ne(const QString& prop, const Value& value);
      QS3I_DECLSPEC unique_ptr<RQLQuery> gt(const QString& prop, const Value& value);
      QS3I_DECLSPEC unique_ptr<RQLQuery> ge(const QString& prop, const Value& value);
      QS3I_DECLSPEC unique_ptr<RQLQuery> lt(const QString& prop, const Value& value);
      QS3I_DECLSPEC unique_ptr<RQLQuery> le(const QString& prop, const Value& value);
      // TODO
      unique_ptr<RQLQuery> in(const QString& prop, const Value& value);
      unique_ptr<RQLQuery> like(const QString& prop, const Value& value);
      unique_ptr<RQLQuery> exists(const QString& prop, const Value& value);

      class QS3I_DECLSPEC LogicalQuery : public RQLQuery {
      public:
         LogicalQuery(unique_ptr<RQLQuery> a, LogicalOperatorType m_operatorType, unique_ptr<RQLQuery> b);
         // Implies 'not'
         LogicalQuery(unique_ptr<RQLQuery> a);

         QString to_string() const override;
      private:
         unique_ptr<RQLQuery> m_a;
         unique_ptr<RQLQuery> m_b;
         LogicalOperatorType m_operatorType;
      };
      // Factory functions
      QS3I_DECLSPEC unique_ptr<RQLQuery> _and(unique_ptr<RQLQuery> a, unique_ptr<RQLQuery> b);
      QS3I_DECLSPEC unique_ptr<RQLQuery> _or(unique_ptr<RQLQuery> a, unique_ptr<RQLQuery> b);
      QS3I_DECLSPEC unique_ptr<RQLQuery> _not(unique_ptr<RQLQuery> a);

      QS3I_DECLSPEC QString url_encode(const QString& str);
   }

}