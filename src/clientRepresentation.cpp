#include "clientRepresentation.h"

#include <QJsonArray>

QJsonObject ClientRepresentation::toJsonObject() const
{
   QJsonObject obj;

   if (!adminUrl.isEmpty())
      obj["adminUrl"] = adminUrl;
   if (!baseUrl.isEmpty())
      obj["baseUrl"] = baseUrl;
   if (!clientAuthenticatorType.isEmpty())
      obj["clientAuthenticatorType"] = clientAuthenticatorType;
   if (!clientId.isEmpty())
      obj["clientId"] = clientId;
   if (!description.isEmpty())
      obj["description"] = description;
   if (!id.isEmpty())
      obj["id"] = id;
   if (!name.isEmpty())
      obj["name"] = name;
   if (!origin.isEmpty())
      obj["origin"] = origin;
   if (!protocol.isEmpty())
      obj["protocol"] = protocol;
   if (!registrationAccessToken.isEmpty())
      obj["registrationAccessToken"] = registrationAccessToken;
   if (!rootUrl.isEmpty())
      obj["rootUrl"] = rootUrl;
   if (!secret.isEmpty())
      obj["secret"] = secret;

   if (!defaultClientScopes.isEmpty())
   {
      QJsonArray arr;
      for (QString elem : defaultClientScopes)
      {
         arr.append(elem);
      }
      obj["defaultClientScopes"] = arr;
   }

   if (!defaultRoles.isEmpty())
   {
      QJsonArray arr;
      for (QString elem : defaultRoles)
      {
         arr.append(elem);
      }
      obj["defaultRoles"] = arr;
   }
   if (!clientScopes.isEmpty())
   {
      QJsonArray arr;
      for (QString elem : clientScopes)
      {
         arr.append(elem);
      }
      obj["clientScopes"] = arr;
   }
   if (!optionalClientScopes.isEmpty())
   {

      QJsonArray arr;
      for (QString elem : optionalClientScopes)
      {
         arr.append(elem);
      }
      obj["optionalClientScopes"] = arr;
   }
   if (!redirectUris.isEmpty())
   {
      QJsonArray arr;
      for (QString elem : redirectUris)
      {
         arr.append(elem);
      }
      obj["redirectUris"] = arr;
   }
   if (!webOrigins.isEmpty())
   {
      QJsonArray arr;
      for (QString elem : webOrigins)
      {
         arr.append(elem);
      }
      obj["webOrigins"] = arr;
   }
   return obj;
}
