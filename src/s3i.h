#ifndef S3I_H
#define S3I_H

#include "qs3i-global.h"
#include "rqlQuery.h"
#include "queryresultiterator.h"
#include "token.h"
#include "thing.h"
#include "authenticationManager.h"
#include "clientRepresentation.h"
#include "brokerInterfaceREST.h"

#include "nlohmann/json.hpp"

#include <QString>
#include <QJsonArray>
#include <QtNetwork/QNetworkAccessManager>

#include <functional>

namespace S3I
{
   using namespace nlohmann;
   // Forward declaration to friend a class from another namespace
   namespace Directory {
      class Thing;
   }

   namespace Policy {
      class PolicyEntry;
   }

   enum class RefreshPolicy {
      BeforeExpiration,
   };

   class QS3I_DECLSPEC S3IManager
   {

   public:
      S3IManager(unique_ptr<AuthenticationManager> authManager);

      // Get a token and keep it fresh
      void login(RefreshPolicy policy = RefreshPolicy::BeforeExpiration);
      // Invalidate the session for the current user
      void logout();

      Directory::Thing getThingById(const QString& id);
      /// Requires leading slash
      json getThingFieldById(const QString& id, const QString& field);
      json getThingFieldsById(const QString& id, const QStringList& fields);
      /// Gets Policy from the directory 
      Policy::PolicyEntry getPolicyById(const QString& id);

      /// Returns the thing id of the user who owns the given thing
      QString getOwnerThingId(const QString& ownedThingId);
      /// Returns the thing of the user who owns the given thing
      Directory::Thing getOwnerThing(const Directory::Thing& ownedThing);

      // TODO QueryResult with Paging
      QList<Directory::Thing> executeQuery(const QString& q);

      QString getDirectoryUrl() const;
      QString getRepositoryUrl() const;

      // AuthManager
      AuthenticationManager * getAuthManager() const;
      void setAuthManager(unique_ptr<AuthenticationManager> authManager);

      /// Interface for Management API. Returns ClientId and ClientSecret of the newly created Thing
      QPair<QString, QString> createThing(const ClientRepresentation& clientRepr);
      void deleteThing(const QString& thingId);
      QString createBrokerEndpoint(const QString& thingId, bool encrypted);

      // Utility
      bool setTokenHeader(QNetworkRequest& req);
      
      /// Waits for the Request to complete, without blocking the QEventLoop
      QNetworkReply* awaitGet(const QNetworkRequest & req);
      /// Waits for the Request to complete, without blocking the QEventLoop
      QNetworkReply* awaitPut(const QNetworkRequest & req, const QByteArray& data);

   private:
      // Could be threadsafe using QMap<QThread, QNetworkAccessManager>
      QNetworkAccessManager m_qnam;
      const QString m_directoryUrl;
      const QString m_repositoryUrl;

      unique_ptr<AuthenticationManager> m_authManager;
   };
}
#endif // S3I_H
