#pragma once

#include <QJsonObject>

struct ResourceServerRepresentation
{

};

struct ProtocolMapperRepresentation
{

};

struct ClientRepresentation
{
   QMap<QString, QString> access;
   QString adminUrl;
   QMap<QString, QString> attributes;
   QMap<QString, QString> authenticationFlowBindingOverrides;
   bool authorizationServicesEnabled;
   ResourceServerRepresentation authorizationSettings;
   QString baseUrl;
   bool bearerOnly;
   QString clientAuthenticatorType;
   QString clientId;
   bool consentRequired;
   QStringList defaultClientScopes;
   QStringList defaultRoles;
   QString description;
   bool directAccessGrantsEnabled;
   bool enabled;
   bool frontchannelLogout;
   bool fullScopeAllowed;
   QString id;
   bool implicitFlowEnabled;
   QString name;
   int nodeReRegistrationTimeout;
   int notBefore;
   QStringList clientScopes;
   QString origin;
   QStringList optionalClientScopes;
   QString protocol;
   QList<ProtocolMapperRepresentation> protocolMappers;
   bool publicClient;
   QStringList redirectUris;
   QMap<QString, QString> registeredNodes;
   QString registrationAccessToken;
   QString rootUrl;
   QString secret;
   bool serviceAccountsEnabled;
   bool standardFlowEnabled;
   bool surrogateAuthRequired;
   QStringList webOrigins;

   QJsonObject toJsonObject() const;
};