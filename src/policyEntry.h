#pragma once

#include "entry.h"
#include "policyGroup.h"
#include "policySubject.h"
#include "policyResource.h"

namespace S3I
{

   namespace Policy
   {
      enum class PolicyFor {
         Directory,
         Repository
      };

      class QS3I_DECLSPEC PolicyEntry : public Entry
      {
      public:
         void commit() const override;
         /// TODO Unclear
         void pull() override;

         QString getETag() const noexcept;
         Group getGroup(QString name) const;
         QMap<QString, Group> getGroups() const noexcept;
         // creates a new Group and adds it to the policyGroups (check if name is unique)
         void insertGroup(const Group& group);
         int deleteGroup(QString name) noexcept;
         /// insert Subject to group 'observer'. Creates the group if not present.
         void insertObserver(const Subject& obs) noexcept;

         //// special and default right groups (owner/observer)
         //// TODO: this method signatures could change, based on the "Zugriffsrechte-Gruppen Diskussion" - there is no specification in the "S�I-Standpunkt" yet
         //QMap<QString, Subject> getOwners();
         //Subject addOwner(QString id);
         //// TODO: prevent deletion of "nginx:ditto" and the user himself? 
         //void deleteOwner(QString id);
         //QMap<QString, Subject> getObservers();

         //void deleteObserver(QString id);

         QJsonObject toJson() const noexcept;

      private:
         // Things are only created by the S3I class
         friend class ::S3I::S3IManager;
         PolicyEntry(S3IManager* s3i, const QString& identifier, const PolicyFor& target);

         static PolicyEntry fromJson(S3IManager* s3i, const QJsonObject& thing, const PolicyFor& target);
         void updateFromJson(const QJsonObject& obj);


         PolicyFor m_policyFor;

         // holds all policy groups (key: Group::name)
         QMap<QString, Group> m_groups;
         // TODO
         // holds the etag from the last cloud update
         // see: https://www.eclipse.org/ditto/httpapi-concepts.html#etag
         QString m_eTag;

      };
   }
}
