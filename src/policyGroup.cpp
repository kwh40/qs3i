#include "policyGroup.h"
#include <stdexcept>

namespace S3I
{
   namespace Policy
   {


      QString Policy::Group::getName() const noexcept
      {
         return m_name;
      }

      QMap<QString, Subject> Policy::Group::getSubjects() const noexcept
      {
         return m_subjects;
      }

      QMap<QString, Resource> Group::getResources() const noexcept
      {
         return m_resources;
      }

      Subject Group::getSubject(QString id) const
      {
         if (!m_subjects.contains(id))
            throw std::invalid_argument("Key Exception");

         return m_subjects[id];
      }

      Resource Group::getResource(QString path) const
      {
         if (!m_resources.contains(path))
            throw std::invalid_argument("Key Exception");

         return m_resources[path];
      }

      void Group::insertSubject(const Subject& sub)
      {
         if (!sub.isValid())
            throw std::invalid_argument("invalid subject");

         m_subjects.insert(sub.getId(), sub);
      }

      void Group::insertResource(const Resource& res)
      {
         if (!res.isValid())
            throw std::invalid_argument("invalid resource");
         m_resources.insert(res.getPath(), res);
      }

      int Group::deleteSubject(QString id) noexcept
      {
         return m_subjects.remove(id);
      }

      int Group::deleteResource(QString path) noexcept
      {
         return m_resources.remove(path);
      }

      bool Group::isValid() const noexcept
      {
         return !m_name.isNull();
      }

      Group::Group(QString name)
         : m_name(name)
      {
      }

      Group::Group(QString name, QMap<QString, Subject>& subjects, QMap<QString, Resource>& resources)
         : m_name(name)
         , m_subjects(subjects)
         , m_resources(resources)
      {
      }

      Group Group::fromJson(const QString & name, const QJsonObject & obj)
      {
         // Example
         //{
         //   "subjects": {
         //      "nginx:/iWald": {
         //         "type": "iWald user group"
         //      },
         //         "nginx:606d8b38-4c3f-46bd-9482-86748e108f32" : {
         //            "type": "nginx basic auth admin user KWH-Team"
         //         }
         //   },
         //      "resources": {
         //      "thing:/": {
         //         "grant": [
         //            "READ"
         //         ],
         //            "revoke" : []
         //      }
         //   }
         //}

         Group group(name);

         if (!obj.contains("subjects") || !obj["subjects"].isObject())
            throw std::invalid_argument("Object contains no field 'subjects' of type object");

         if (!obj.contains("resources") || !obj["resources"].isObject())
            throw std::invalid_argument("Object contains no field 'resources' of type object");

         const auto subjectsObj = obj["subjects"].toObject();
         for (const QString& key : subjectsObj.keys())
         {
            QJsonValue value = subjectsObj.value(key);
            try {
               group.insertSubject(Subject::fromJson(key, value.toObject()));
            }
            catch (std::invalid_argument e) {
               // do nothing
            }
         }

         const auto resourcesObj = obj["resources"].toObject();
         for (const QString& key : resourcesObj.keys())
         {
            QJsonValue value = resourcesObj.value(key);
            try {
               group.insertResource(Resource::fromJson(key, value.toObject()));
            }
            catch (std::invalid_argument e) {
               // do nothing
            }
         }

         return group;
      }

      QJsonObject Group::toJson() const noexcept
      {
         QJsonObject obj;


         QJsonObject subjects;
         for (const auto subName : m_subjects.keys())
         {
            subjects[subName] = m_subjects[subName].toJson();
         }
         obj["subjects"] = subjects;


         QJsonObject resources;
         for (const auto resName : m_resources.keys())
         {
            resources[resName] = m_resources[resName].toJson();
         }
         obj["resources"] = resources;

         return obj;
      }

   }
}