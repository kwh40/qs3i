#include "rqlQuery.h"

#include <QUrl>

//#include <cctype>
//#include <iomanip>
//#include <sstream>
namespace S3I
{
   namespace RQL {

      Value::Value(const QString& value)
         : m_valueType(ValueType::_STRING)
         , m_stringValue(value)
      {}

      Value::Value(bool value)
         : m_valueType(ValueType::_BOOL)
         , m_boolValue(value)
      {}

      Value::Value(int value)
         : m_valueType(ValueType::_INT)
         , m_intValue(value)
      {}

      Value::Value(double value)
         : m_valueType(ValueType::_DOUBLE)
         , m_doubleValue(value)
      {}

      Value::Value()
         : m_valueType(ValueType::_NULL)
      {}

      Value Value::_NULL() {
         return Value();
      }

      QString Value::to_string() const
      {
         QString value;

         if (m_valueType == ValueType::_STRING)
         {
            value = m_stringValue;
         }
         else if (m_valueType == ValueType::_INT)
         {
            value = QString::number(m_intValue);
         }
         else if (m_valueType == ValueType::_DOUBLE)
         {
            value = QString::number(m_doubleValue);
         }
         else if (m_valueType == ValueType::_BOOL)
         {
            value = QString(m_boolValue);
         }
         else // (m_valueType == ValueType::_NULL)
         {
            value = "null";
         }

         value.prepend('"');
         value.append('"');
         return value;
      }

      RelationalQuery::RelationalQuery(const QString& prop, RelationalOperatorType op, const Value& value)
         : m_property(prop)
         , m_value(value)
         , m_operatorType(op)
      {}

      unique_ptr<RQLQuery> eq(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::eq, value);
         return qe;
      }

      unique_ptr<RQLQuery> ne(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::ne, value);
         return qe;
      }
      unique_ptr<RQLQuery> gt(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::gt, value);
         return qe;
      }
      unique_ptr<RQLQuery> ge(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::ge, value);
         return qe;
      }
      unique_ptr<RQLQuery> lt(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::lt, value);
         return qe;
      }
      unique_ptr<RQLQuery> le(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::le, value);
         return qe;
      }
      unique_ptr<RQLQuery> in(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::in, value);
         return qe;
      }
      unique_ptr<RQLQuery> like(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::like, value);
         return qe;
      }
      unique_ptr<RQLQuery> exists(const QString& prop, const Value& value)
      {
         unique_ptr<RQLQuery> qe = make_unique<RelationalQuery>(prop, RelationalOperatorType::exists, value);
         return qe;
      }

      QString RelationalQuery::to_string() const
      {
         return RQL::to_string(m_operatorType) + "(" + m_property + "," + m_value.to_string() + ")";
      }

      LogicalQuery::LogicalQuery(
         unique_ptr<RQLQuery> a,
         LogicalOperatorType operatorType,
         unique_ptr<RQLQuery> b)
         : m_a(std::move(a))
         , m_b(std::move(b))
         , m_operatorType(operatorType)
      {}

      LogicalQuery::LogicalQuery(unique_ptr<RQLQuery> a)
         : m_a(std::move(a))
         , m_b()
         , m_operatorType(LogicalOperatorType::NOT)
      {}

      unique_ptr<RQLQuery> _and(unique_ptr<RQLQuery> a, unique_ptr<RQLQuery> b)
      {
         unique_ptr<RQLQuery> qe = make_unique<LogicalQuery>(std::move(a), LogicalOperatorType::AND, std::move(b));
         return qe;
      }

      unique_ptr<RQLQuery> _or(unique_ptr<RQLQuery> a, unique_ptr<RQLQuery> b)
      {
         unique_ptr<RQLQuery> qe = make_unique<LogicalQuery>(std::move(a), LogicalOperatorType::OR, std::move(b));
         return qe;
      }

      unique_ptr<RQLQuery> _not(unique_ptr<RQLQuery> a)
      {
         unique_ptr<RQLQuery> qe = make_unique<LogicalQuery>(std::move(a));
         return qe;
      }

      QString LogicalQuery::to_string() const
      {
         if (m_operatorType == LogicalOperatorType::NOT)
         {
            return RQL::to_string(m_operatorType) + "(" + m_a->to_string() + ")";
         }
         else
         {
            return RQL::to_string(m_operatorType) + "(" + m_a->to_string() + "," + m_b->to_string() + ")";
         }
      }

      QString to_string(RelationalOperatorType operatorType)
      {
         if (operatorType == RelationalOperatorType::eq) { return "eq"; }
         else if (operatorType == RelationalOperatorType::eq) { return "eq"; }
         else if (operatorType == RelationalOperatorType::ne) { return "ne"; }
         else if (operatorType == RelationalOperatorType::gt) { return "gt"; }
         else if (operatorType == RelationalOperatorType::ge) { return "ge"; }
         else if (operatorType == RelationalOperatorType::lt) { return "lt"; }
         else if (operatorType == RelationalOperatorType::le) { return "le"; }
         else if (operatorType == RelationalOperatorType::in) { return "in"; }
         else if (operatorType == RelationalOperatorType::like) { return "like"; }
         else if (operatorType == RelationalOperatorType::exists) { return "exists"; }
         else { return "invalid OperatorType"; }
      }

      QString to_string(LogicalOperatorType operatorType)
      {
         if (operatorType == LogicalOperatorType::AND) { return "and"; }
         else if (operatorType == LogicalOperatorType::OR) { return "or"; }
         else if (operatorType == LogicalOperatorType::NOT) { return "not"; }
         else { return "invalid OperatorType"; }
      }

      QString url_encode(const QString& value) {
         return QUrl::toPercentEncoding(value);
      }
   }

}