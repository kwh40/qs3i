#pragma once

#include "qs3i-global.h"

#include <qnetworkreply.h>

namespace S3I
{
   class QS3I_DECLSPEC Exception : public std::exception
   {
   public:
      Exception(const QString& message);
      virtual QString longWhat() const noexcept;

   protected:
      QString m_msg;
   };

   /// what return QNetworkReply->errorString()
   class QS3I_DECLSPEC NetworkException : public Exception
   {
   public:
      NetworkException(QNetworkReply* reply);

      QString longWhat() const noexcept override;
      const char* what() const override;

      QNetworkReply::NetworkError error;
      QByteArray errorString; // To be able to return const char* in std::exception::what()
      QString serverResponse;
      QNetworkRequest request;
   };

   class QS3I_DECLSPEC MaxRetryException : public Exception
   {
   public:
      MaxRetryException(const QString& message);
   };

   class QS3I_DECLSPEC AuthException : public Exception
   {

   };
   
   class QS3I_DECLSPEC ParseException : public Exception
   {
   public:
      ParseException(const QString& message);
   };

   class QS3I_DECLSPEC JWTParseException : public ParseException
   {
   public:
      JWTParseException(const QString& message);
   };

   class QS3I_DECLSPEC InvalidJsonException : public ParseException
   {
   public:
      InvalidJsonException(const QString& message);
   };

   class QS3I_DECLSPEC SchemaException : public ParseException
   {
   public:
      SchemaException(const QString& message);
   };

   class QS3I_DECLSPEC KeyException : public Exception
   {
   public:
      KeyException(const QString& message);
   };
}
