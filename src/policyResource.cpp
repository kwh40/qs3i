#include "policyResource.h"
#include "utils.h"

#include <stdexcept>
#include <QJsonObject>
#include <QJsonArray>
namespace S3I
{
   namespace Policy
   {

      QString Resource::getPath() const noexcept
      {
         return m_path;
      }

      QSet<PermissionType> Resource::getGrant() const noexcept
      {
         return m_grant;
      }

      QSet<PermissionType> Resource::getRevoke() const noexcept
      {
         return m_revoke;
      }

      void Resource::setGrant(QSet<PermissionType> grant) noexcept
      {
         m_grant = grant;
      }

      void Resource::setRevoke(QSet<PermissionType> revoke) noexcept
      {
         m_revoke = revoke;
      }

      QJsonObject Resource::toJson() const noexcept
      {
         QJsonObject obj;

         QJsonArray grants;
         for (const auto g : m_grant)
         {
            grants.append(stringFromPermissionType(g));
         }
         obj["grant"] = grants;

         QJsonArray revokes;
         for (const auto g : m_revoke)
         {
            revokes.append(stringFromPermissionType(g));
         }
         obj["revoke"] = revokes;

         return obj;
      }

      bool Resource::isValid() const noexcept
      {
         return !m_path.isNull();
      }

      Resource::Resource(QString path, QSet<PermissionType> grant, QSet<PermissionType> revoke)
         : m_path(path)
         , m_grant(grant)
         , m_revoke(revoke)
      {
      }

      Resource Resource::fromJson(const QString& path, const QJsonObject & obj) noexcept
      {
         //{
         //   "grant": [
         //      "READ",
         //      "WRITE"
         //   ],
         //   "revoke" : []
         //}

         // TODO check everything
         QSet<PermissionType> grant;
         for (const auto pt : obj["grant"].toArray())
         {
            try {
               grant.insert(permissionTypeFromString(pt.toString()));
            }
            catch (std::invalid_argument e) {
               // do nothing
            }
         }

         QSet<PermissionType> revoke;
         for (const auto pt : obj["revoke"].toArray())
         {
            try {
               revoke.insert(permissionTypeFromString(pt.toString()));
            }
            catch (std::invalid_argument e) {
               // do nothing
            }
         }

         return Resource(path, grant, revoke);
      }
      PermissionType Resource::permissionTypeFromString(const QString & pt)
      {
         if (pt == "READ")
            return PermissionType::Read;
         if (pt == "WRITE")
            return PermissionType::Write;
         if (pt == "EXECUTE")
            return PermissionType::Execute;

         throw std::invalid_argument("");
      }

      QString Resource::stringFromPermissionType(const PermissionType & pt) noexcept
      {
         switch (pt)
         {
         case PermissionType::Read:
            return "READ";
         case PermissionType::Write:
            return "WRITE";
         default:
            return "EXECUTE";
         }
      }
   }
}