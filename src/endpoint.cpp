#include "endpoint.h"

namespace S3I
{
   namespace Directory {

      Endpoint::Endpoint(const QString& uri)
         : m_uri(uri)
      {

      }

      Endpoint::operator QJsonValue() const
      {
         return QJsonValue(m_uri);
      }

      Endpoint::operator QString() const
      {
         return m_uri;
      }
   }
}