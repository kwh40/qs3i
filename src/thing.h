#ifndef THING_H
#define THING_H

#include "qs3i-global.h"
#include "endpoint.h"
#include "entry.h"
#include "role.h"

#include <QJsonObject>
#include <QList>

#include <memory>

namespace S3I
{

   class S3IManager;
   class Query;

   namespace Directory {

      enum ThingType {
         Component,
         Service,
         Hmi,
         Undefined
      };

      ThingType thingTypeFromString(const QString& thingType);
      QString stringFromThingType(const ThingType& thingType);

      class DirValue
      {
      public:
         DirValue() = default;
         static DirValue fromJson(const QJsonObject& obj);

         QJsonObject toJson() const;
         operator QJsonValue() const;

         QString attribute;
         QStringList value; //TODO Why not QJsonValue?
      };

      class DirObject;

      class DirLink
      {
      public:
         DirLink() = default;
         static DirLink fromJson(const QJsonObject& obj);
         QJsonObject toJson() const;
         operator QJsonValue() const;

         QString association;
         std::shared_ptr<DirObject> target;
      };

      class DirObject 
      {
      public:
         DirObject() = default;
         static DirObject fromJson(const QJsonObject& obj);
         QJsonObject toJson() const;
         operator QJsonValue() const;
         
         bool isValid() const noexcept;

         QString identifier;
         QString clazz;
         QList<DirLink> links;
         QList<DirValue> values;
      };

      class QS3I_DECLSPEC Thing : public Entry
      {
      public:
         Thing(const Thing& other) = default;
         Thing& operator=(const Thing& other);

         /// commits local changes to the s3i cloud - PUT to /things/{thingId}
         void commit() const override;

         QString dataModel;
         QString location;
         QString name;
         QString defaultHMI;
         QString ownedBy;
         QString administratedBy;
         QString usedBy;
         QString represents;

         QByteArray publicKey;
         ThingType thingType;
         QList<Endpoint> allEndpoints;
         Endpoint defaultEndpoint;
         DirObject thingStructure;

         QJsonObject toJson() const;

         QList<DirObject*> getFeaturesByClassName(const QString& className) const noexcept;
         /// raises KeyException
         QStringList& getFirstFeatureValueMatchingClassAndAttribute(const QString & clazz, const QString& attribute) const;
         /// clazz and attribute is ambiguous. Iterates over all features matching clazz and returns the first matching attribute
         /// If none match clazz and attribute, attribute will be created in the first feature matching clazz
         QStringList& getOrCreateFirstFeatureValueMatchingClassAndAttribute(const QString & clazz, const QString& attribute) noexcept;

      private:
         // Things are only created by the S3I class
         friend class ::S3I::S3IManager;
         static Thing fromJson(S3IManager* s3i, const QJsonObject& thing);
         void updateFromJson(const QJsonObject& obj);

         Thing(S3IManager* s3i, const QString& identifier);
      };
   }
}
#endif // THING_H
