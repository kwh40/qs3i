#ifndef S3ICPPQT_GLOBAL_H
#define S3ICPPQT_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef QS3I_EXPORT
#  define QS3I_DECLSPEC Q_DECL_EXPORT
#else
#  define QS3I_DECLSPEC Q_DECL_IMPORT
#endif

#endif // S3ICPPQT_GLOBAL_H
