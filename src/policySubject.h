#pragma once

#include "qs3i-global.h"

#include <QDateTime>
#include <QJsonObject>

namespace S3I
{
   namespace Policy
   {
      class QS3I_DECLSPEC Subject
      {
      public:
         Subject() = default;
         Subject(QString id) noexcept;
         static Subject fromJson(QString id, const QJsonObject& obj) noexcept;

         QString getId() const noexcept;
         QDateTime getExpiringTimestamp() const noexcept;
         QString getType() const noexcept;
         void setExpiringTimestamp(const QDateTime& expiringTimestamp) noexcept;
         void setType(const QString& t) noexcept;

         QJsonObject toJson() const noexcept;
         bool isValid() const noexcept;

      private:
         // subject id (s3i identifier/ (s3i-idp-)group name / user id, use "nginx:<ID>")
         QString m_id;
         // stores the validation date of this subject (optional) 
         // use an external lib (here QDateTime) and convert to an ISO-8601 string for the request
         QDateTime m_expiringTimestamp;
         // subject description (optional)
         QString m_type;

      };
   }
}