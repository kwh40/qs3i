#pragma once
namespace S3I
{
   namespace Policy
   {
      enum PermissionType {
         Read,
         Write,
         Execute
      };

      class Resource
      {

      public:
         Resource() = default;
         Resource(QString path, QSet<PermissionType> grant, QSet<PermissionType> revoke);
         static Resource fromJson(const QString& path, const QJsonObject& obj) noexcept;
         static PermissionType permissionTypeFromString(const QString& pt);
         static QString stringFromPermissionType(const PermissionType& pt) noexcept;

         QString getPath() const noexcept;
         QSet<PermissionType> getGrant() const noexcept;
         QSet<PermissionType> getRevoke() const noexcept;
         void setGrant(QSet<PermissionType> grant) noexcept;
         void setRevoke(QSet<PermissionType> revoke) noexcept;

         QJsonObject toJson() const noexcept;
         bool isValid() const noexcept;

      private:
         // path to the resource (e.g.: "thing:/", "policy:/", "thing:/features/featureX/properties/location")
         QString m_path;
         // allowed actions on this resource
         QSet<PermissionType> m_grant;
         // permitted actions on this resource
         QSet<PermissionType> m_revoke;
      };

   }
}