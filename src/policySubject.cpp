#include "policySubject.h"
namespace S3I
{
   QString Policy::Subject::getId() const noexcept
   {
      return m_id;
   }

   QDateTime Policy::Subject::getExpiringTimestamp() const noexcept
   {
      return m_expiringTimestamp;
   }

   QString Policy::Subject::getType() const noexcept
   {
      return m_type;
   }

   void Policy::Subject::setExpiringTimestamp(const QDateTime & expiringTimestamp) noexcept
   {
      m_expiringTimestamp = expiringTimestamp;
   }

   void Policy::Subject::setType(const QString & t) noexcept
   {
      m_type = t;
   }

   Policy::Subject::Subject(QString id) noexcept
      : m_id(id)
   {
   }

   Policy::Subject Policy::Subject::fromJson(QString id, const QJsonObject & obj) noexcept
   {
      Subject sub(id);

      if (obj.contains("type") && obj["type"].isString())
         sub.setType(obj["type"].toString());

      if (obj.contains("expiry") && obj["expiry"].isString())
         sub.setExpiringTimestamp(QDateTime::fromString(obj["expiry"].toString(), Qt::DateFormat::ISODate));

      return sub;
   }

   QJsonObject Policy::Subject::toJson() const noexcept
   {
      QJsonObject obj;

      if (!m_type.isNull())
         obj["type"] = m_type;
      if (m_expiringTimestamp.isValid())
         obj["expiry"] = m_expiringTimestamp.toString(Qt::DateFormat::ISODate);

      return obj;
   }

   bool Policy::Subject::isValid() const noexcept
   {
      return !m_id.isNull();
   }
}