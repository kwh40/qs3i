#ifndef TOKEN_H
#define TOKEN_H

#include "qs3i-global.h"

#include <QString>
#include <QJsonObject>
#include <QDateTime>
namespace S3I
{
   class QS3I_DECLSPEC Token
   {
   public:

      Token();
      Token(const QString& base64);

      /// No 'exp' field is interpreted as no expiration or infinite validity
      /// Throws Schema Exception
      QDateTime getExpiration() const noexcept;
      /// Only meaningful if the token has an 'exp' field or is a default s3i offline token (30 days lifetime) 
      /// Throws Schema Exception
      qint64 secsToExpiration() const noexcept;
      /// Throws Schema Exception
      bool isExpired() const noexcept;
      bool isEmpty() const noexcept;
      QJsonValue getClaim(const QString& claim) const;
      QString encoded() const;

   private:
      Token(const QJsonObject& header, const QJsonObject& payload, const QString& token);
      QJsonObject m_header;
      QJsonObject m_payload;
      QString m_token;
   };

   class QS3I_DECLSPEC AccessToken : public Token
   {
   public:
      // TODO Check if it is actually an AccessToken
      AccessToken();
      AccessToken(const QString& base64);
   };

   class QS3I_DECLSPEC RefreshToken : public Token
   {
   public:
      // TODO Check if it is actually a RefreshToken
      RefreshToken();
      RefreshToken(const QString& base64);
   };
}
#endif // TOKEN_H
