#ifndef ENTRY_H
#define ENTRY_H
#include "qs3i-global.h"

namespace S3I
{

   class S3IManager;

   class QS3I_DECLSPEC Entry
   {
   public:
      Entry(S3IManager* s3i, const QString& identifier);

      /// commits local changes to the s3i cloud - PUT to /things/{thingId}
      virtual void commit() const = 0;
      /// TODO Unclear
      virtual void pull() {};

      const QString identifier;

   protected:
      S3IManager* m_s3i;


   };

}

#endif // ENTRY_H
