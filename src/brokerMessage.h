#pragma once

#include "qs3i-global.h"

#include <QPointer>
#include <QFile>
#include <QDateTime>

#include <memory>

#include "nlohmann/json.hpp"

namespace S3I
{
   namespace Broker
   {
      using namespace nlohmann;

      class QS3I_DECLSPEC Message
      {
      public:
         Message(const QStringList& receivers, const QString& replyToEndpoint, const QString& sender, const QString& replyingToMessage = QString());
         Message();
         // encrypted message
         QString m_identifier;
         QStringList m_receivers;
         QString m_replyingToMessage;
         //QStringList m_replyToEndpoints;
         QString m_replyToEndpoint;
         QString m_sender;

         virtual json toJson() const;
         virtual ~Message();
         virtual QByteArray toQByteArray() const;

         static std::shared_ptr<Message> createMessage(const json& message);

      protected:
         static Message fromJson(const json& json);

      private:
         QString createMessageIdentifier();
      };

      class QS3I_DECLSPEC UserMessage : public Message
      {
      public:
         UserMessage(
             const QStringList& receivers, 
             const QString& replyToEndpoint, 
             const QString& sender, 
             const QString& subject, 
             const QString& text, 
             const QString& replyingToMessage = QString(), 
             QFile* attachement = nullptr
         );
         UserMessage();
         json toJson() const override;

         void addAttachement();
         void addAttachement(QFile* attachment);
         bool deleteAttachment(const QString& name);
         void addAttachment_fromQByteArray(const QString& name, const QByteArray& attachement);

         static UserMessage fromJson(json json);

         QString m_subject;
         QString m_text;
         QMap<QString, QByteArray> m_attachements;
         QVector<QPointer<QFile>> m_addedAttachements;
      };


      class QS3I_DECLSPEC AttributeValueMessage : public Message
      {
      public:
         AttributeValueMessage() = default;
         AttributeValueMessage(const Message& baseMessage);
         json toJson() const override;

      protected:
         static AttributeValueMessage fromJson(const json& json);
      };

      class QS3I_DECLSPEC GetValueMessage : public AttributeValueMessage
      {
      public:
         GetValueMessage() = default;
         GetValueMessage(const AttributeValueMessage& baseMessage);
      };

      class QS3I_DECLSPEC GetValueRequest : public GetValueMessage
      {
      public:
         GetValueRequest() = default;
         GetValueRequest(const GetValueMessage& baseMessage);
         json toJson() const override;

         static GetValueRequest fromJson(const json& json);

         QString m_attributePath;
      };

      class QS3I_DECLSPEC GetValueReply : public GetValueMessage
      {
      public:
         GetValueReply() = default;
         GetValueReply(const GetValueMessage& baseMessage);
         json toJson() const override;

         static GetValueReply fromJson(const json& json);

         json m_value;
      };

      class QS3I_DECLSPEC SetValueMessage : public AttributeValueMessage
      {
      public:
         SetValueMessage() = default;
         SetValueMessage(const AttributeValueMessage& baseMessage);
      };

      class QS3I_DECLSPEC SetValueRequest : public SetValueMessage
      {
      public:
         SetValueRequest() = default;
         SetValueRequest(const SetValueMessage& baseMessage);
         json toJson() const override;

         static SetValueRequest fromJson(const json& json);

         QString m_attributePath;
         json m_newValue;
      };

      class QS3I_DECLSPEC SetValueReply : public SetValueMessage
      {
      public:
         SetValueReply() = default;
         SetValueReply(const SetValueMessage& baseMessage);
         json toJson() const override;

         static SetValueReply fromJson(const json& json);

         bool m_ok;
      };

      class QS3I_DECLSPEC DeleteAttributeMessage : public Message
      {
      public:
         DeleteAttributeMessage() = default;
         DeleteAttributeMessage(const Message& baseMessage);
      };

      class QS3I_DECLSPEC DeleteAttributeRequest : public DeleteAttributeMessage
      {
      public:
         DeleteAttributeRequest() = default;
         DeleteAttributeRequest(const DeleteAttributeMessage& baseMessage);
         json toJson() const override;

         static DeleteAttributeRequest fromJson(const json& json);

         QString m_attributePath;
      };

      class QS3I_DECLSPEC DeleteAttributeReply : public DeleteAttributeMessage
      {
      public:
         DeleteAttributeReply() = default;
         DeleteAttributeReply(const DeleteAttributeMessage& baseMessage);
         json toJson() const override;

         static DeleteAttributeReply fromJson(const json& json);

         bool m_ok;
      };

      class QS3I_DECLSPEC CreateAttributeMessage : public Message
      {
      public:
         CreateAttributeMessage() = default;
         CreateAttributeMessage(const Message& baseMessage);
      };

      class QS3I_DECLSPEC CreateAttributeRequest : public CreateAttributeMessage
      {
      public:
         CreateAttributeRequest() = default;
         CreateAttributeRequest(const CreateAttributeMessage& baseMessage);
         json toJson() const override;

         static CreateAttributeRequest fromJson(const json& json);

         QString m_attributePath;
         json m_newValue;
      };

      class QS3I_DECLSPEC CreateAttributeReply : public CreateAttributeMessage
      {
      public:
         CreateAttributeReply() = default;
         CreateAttributeReply(const CreateAttributeMessage& baseMessage);
         json toJson() const override;

         static CreateAttributeReply fromJson(const json& json);

         bool m_ok;
      };

      class QS3I_DECLSPEC ServiceMessage : public Message
      {
      public:
         ServiceMessage() = default;
         ServiceMessage(const Message& baseMessage);
         ServiceMessage(
            const QStringList& receivers,
            const QString& sender,
            const QString& serviceType,
            const QString& replyToEndpoint = QString(),
            const QString& replyingToMessage = QString()
         );
         json toJson() const override;

         QString m_serviceType;
      protected:
         static ServiceMessage fromJson(const json& json);

      };

      class QS3I_DECLSPEC ServiceRequest : public ServiceMessage
      {
      public:
         ServiceRequest() = default;
         ServiceRequest(const QStringList& receivers,
            const QString& sender,
            const QString& serviceType,
            json parameters = json(),
            const QString& replyToEndpoint = QString(),
            const QString& replyingToMessage = QString()
         );
         ServiceRequest(const ServiceMessage& baseMessage);
         json toJson() const override;

         static ServiceRequest fromJson(const json& json);
         json m_parameters;
      };

      class QS3I_DECLSPEC ServiceReply : public ServiceMessage
      {
      public:
         ServiceReply(const QStringList& receivers,
            const QString& sender,
            const QString& serviceType,
            json results = json(),
            const QString& replyToEndpoint = QString(),
            const QString& replyingToMessage = QString()
         );
         ServiceReply() = default;
         ServiceReply(const ServiceMessage& baseMessage);

         json toJson() const override;

         static ServiceReply fromJson(const json& json);
         json m_results;
      };

      class QS3I_DECLSPEC ServiceRequestStatus : public ServiceMessage
      {
      public:
         ServiceRequestStatus() = default;
         ServiceRequestStatus(const QStringList& receivers,
            const QString& sender,
            const QString& serviceType,
            const QString& replyingToMessage,
            json statusInformation = json()
         );
         ServiceRequestStatus(const ServiceMessage& baseMessage);
         json toJson() const override;

         static ServiceRequestStatus fromJson(const json& json);
         json m_statusInformation;
      };

      class QS3I_DECLSPEC EventMessage : public Message
      {
      public:
         EventMessage() = default;
         EventMessage(
            const QString& sender,
            const QString& topic,
            const QDateTime& timestamp = QDateTime::currentDateTime(),
            json content = json()
         );
         EventMessage(const Message& baseMessage);
         EventMessage(const EventMessage& other) = default;

         json toJson() const override;
         static EventMessage fromJson(const json& json);
         
         QString topic;
         QDateTime timestamp;
         json content;
      };

      class QS3I_DECLSPEC CustomEventRequest : public Message
      {
      public:
         CustomEventRequest() = default;
         CustomEventRequest(const QStringList& receivers,
            const QString& replyToEndpoint,
            const QString& sender,
            const QString& filter,
            const QStringList& fields
         );
         CustomEventRequest(const Message& baseMessage);
         CustomEventRequest(const CustomEventRequest& other) = default;

         json toJson() const override;
         static CustomEventRequest fromJson(const json& json);

         QString filter;
         QStringList attributePaths;
      };

      class QS3I_DECLSPEC CustomEventReply : public Message
      {
      public:
         CustomEventReply() = default;
         CustomEventReply(const QStringList& receivers,
            const QString& sender,
            const QString& status,
            const QString& topic
         );
         CustomEventReply(const Message& baseMessage);
         CustomEventReply(const CustomEventReply& other) = default;

         json toJson() const override;
         static CustomEventReply fromJson(const json& json);

         QString status;
         QString topic;
      };
   }
}

Q_DECLARE_METATYPE(S3I::Broker::GetValueRequest);

