#pragma once

#if defined(__unix) || defined(__QNXNTO__)
    #define S3ICppExport
#else
    #ifdef S3ICPP_EXPORT
        #define S3ICppExport __declspec(dllexport)
    #else
        #define S3ICppExport __declspec(dllimport)
    #endif //ifdef S3ICPP_EXPORT
#endif //if defined(__unix) || defined(__QNXNTO__)
