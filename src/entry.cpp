#include "entry.h"
#include "s3i.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkRequest>
#include <QNetworkReply>


namespace S3I
{

   Entry::Entry(S3IManager* s3i, const QString& id)
      : identifier(id)
      , m_s3i(s3i)
   {

   }
}