#include "brokerMessage.h"
#include "exception.h"
#include "utils.h"

#include <quuid.h>

#ifdef VS_WIDGETS
   #include <QFileDialog>
#endif

namespace S3I
{
   namespace Broker
   {
      using namespace nlohmann;

      Message::Message(const QStringList & receivers, const QString & replyToEndpoint, const QString & sender, const QString & replyingToMessage)
         : m_receivers(receivers)
         , m_replyingToMessage(replyingToMessage)
         , m_replyToEndpoint(replyToEndpoint)
         , m_sender(sender)
         , m_identifier(createMessageIdentifier())
      {

      }


      Message::Message()
         : m_identifier(createMessageIdentifier())
      {

      }

      Message::~Message()
      {

      }

      QByteArray Message::toQByteArray() const
      {
         return toJson().dump().c_str();
      }

      std::shared_ptr<Message> Message::createMessage(const json& message)
      {
         if (message["messageType"] == "userMessage") {
            return std::make_shared<UserMessage>(UserMessage::fromJson(message));
         }
         if (message["messageType"] == "serviceReply") {
            ServiceReply value = ServiceReply::fromJson(message);
            return std::make_shared<ServiceReply>(value);
         }
         if (message["messageType"] == "serviceRequest") {
            ServiceRequest value = ServiceRequest::fromJson(message);
            return std::make_shared<ServiceReply>(value);
         }
         if (message["messageType"] == "serviceRequestStatus") {
            ServiceRequestStatus value = ServiceRequestStatus::fromJson(message);
            return std::make_shared<ServiceRequestStatus>(value);
         }
         return nullptr;
      }

      Message Message::fromJson(const json & json)
      {
         auto obj = json;
         Message message;

         if (obj.contains("identifier"))
         {
            auto val = obj["identifier"];
            if (val.is_string())
               message.m_identifier = val.get<std::string>().c_str();
         }
         if (obj.contains("receivers"))
         {
            auto val = obj["receivers"];
            if (val.is_array())
            {
               for (auto entry : val)
               {
                  if (entry.is_string())
                     message.m_receivers << entry.get<std::string>().c_str();
               }
            }
         }
         if (obj.contains("sender"))
         {
            auto val = obj["sender"];
            if (val.is_string())
               message.m_sender = val.get<std::string>().c_str();
         }
         if (obj.contains("replyToEndpoint"))
         {
            auto val = obj["replyToEndpoint"];
            if (val.is_string())
               message.m_replyToEndpoint = val.get<std::string>().c_str();
         }
         if (obj.contains("replyingToMessage"))
         {
            auto val = obj["replyingToMessage"];
            if (val.is_string())
               message.m_replyingToMessage = val.get<std::string>().c_str();
         }
         return message;
      }

      QString Message::createMessageIdentifier()
      {
         auto uuidString = QUuid::createUuid().toString();
         uuidString.replace(0, 1, "");
         auto identifier = "s3i:" + uuidString.left(uuidString.count() - 1);;
         return identifier;
      }


      json Message::toJson() const
      {
         json obj;
         obj["identifier"] = m_identifier.toUtf8();
         for (QString element : m_receivers)
         {
            obj["receivers"].push_back(element.toUtf8());
         }
         obj["replyingToMessage"] = m_replyingToMessage.toUtf8();
         if (!m_replyToEndpoint.isEmpty())
         {
            obj["replyToEndpoint"] = m_replyToEndpoint.toUtf8();
         }
         obj["sender"] = m_sender.toUtf8();
         return obj;
      }

      UserMessage::UserMessage()
      {
         m_identifier = QUuid::createUuid().toString();
      }

      UserMessage::UserMessage(const QStringList& receivers, const QString& replyToEndpoint, const QString& sender, const QString& subject, const QString& text, const QString& replyingToMessage, QFile *attachement)
         : Message(receivers, replyToEndpoint, sender, replyingToMessage)
         , m_subject(subject)
         , m_text(text)
      {
         if (attachement) m_addedAttachements.append(attachement);
      }

      json UserMessage::toJson() const
      {
         auto message = Message::toJson();
         message["subject"] = m_subject.toUtf8();
         message["text"] = m_text.toUtf8();
         message["messageType"] = "userMessage";

         if (m_attachements.size() != 0 || m_addedAttachements.size() != 0)
         {
            json _array = message["attachments"];

            for (int i = 0; i < m_addedAttachements.size(); i++)
            {
               if (m_addedAttachements.at(i)->open(QIODevice::ReadOnly))
               {
                  const QByteArray attachement = m_addedAttachements.at(i)->readAll();
                  m_addedAttachements.at(i)->close();

                  json obj;
                  obj["filename"] = Utils::getFilenameFromPath(m_addedAttachements.at(i)->fileName()).toUtf8();
                  obj["data"] = attachement.toBase64();
                  _array.push_back(obj);
               }
               else
               {
                  qDebug() << "UserMessage::toJson -> Can not open attachment no." << i;
               }
            }

            for (const QString& name : m_attachements.keys())
            {
               json obj;
               obj["filename"] = name.toUtf8();
               obj["data"] = m_attachements.value(name).toBase64();
               _array.push_back(obj);
            }

            message["attachments"] = _array;
            qDebug() << "Message attachments: " << message["attachments"].dump().c_str();

         }
         return message;
      }

      UserMessage UserMessage::fromJson(json inputJson)
      {
         UserMessage usrMsg;
         usrMsg.m_identifier = inputJson["identifier"].get<std::string>().c_str();
         json receivers_array = inputJson["receivers"];
         for (int i = 0; i != receivers_array.size(); i++) //QJsonArray::const_iterator i = receivers_array.constBegin(); i != receivers_array.constEnd(); i++
         {
            usrMsg.m_receivers.append(receivers_array.at(i).get<std::string>().c_str());
         }
         if (inputJson.contains("replyingToMessage"))
            usrMsg.m_replyingToMessage = inputJson["replyingToMessage"].get<std::string>().c_str();
         //QJsonArray replyToEndpoints_array = json["replyingToEndpoint"].toArray();
         //for (int i = 0; i != replyToEndpoints_array.size(); i++)
         //{
         //   usrMsg.m_replyToEndpoints.append(replyToEndpoints_array.at(i).get<std::string>().c_str());
         //}
         //QJsonArray replyToEndpoints_array = json["replyingToEndpoint"].get<std::string>().c_str();
         usrMsg.m_replyToEndpoint = inputJson["replyToEndpoint"].get<std::string>().c_str();
         usrMsg.m_sender = inputJson["sender"].get<std::string>().c_str();
         usrMsg.m_subject = inputJson["subject"].get<std::string>().c_str();
         usrMsg.m_text = inputJson["text"].get<std::string>().c_str();
         json attachments = inputJson["attachments"];
         //attachments = attachments.at(0);

         for (const auto attachment : attachments) {
            const QString name = attachment["filename"].get<std::string>().c_str();
            const QByteArray data = QByteArray::fromBase64(attachment["data"].get<std::string>().c_str());
            usrMsg.m_attachements.insert(name, data);
         }
         return usrMsg;
      }



      void UserMessage::addAttachement()
      {
#ifdef VS_WIDGETS
         m_addedAttachements.append(new QFile(QFileDialog::getOpenFileName(nullptr, "Get Attachement")));
#endif
      }

      void UserMessage::addAttachement(QFile * attachment)
      {
         m_addedAttachements.append(attachment);
      }

      bool UserMessage::deleteAttachment(const QString& name)
      {
         if (m_attachements.contains(name))
         {
            m_attachements.remove(name);
         }

         for (int i = 0; i < m_addedAttachements.size(); i++)
         {
            if (Utils::getFilenameFromPath(m_addedAttachements.at(i)->fileName()) == name)
            {
               delete m_addedAttachements.at(i);
               m_addedAttachements.remove(i);
               return true;
            }
         }
         return false;
      }

      void UserMessage::addAttachment_fromQByteArray(const QString& name, const QByteArray & attachement)
      {
         m_attachements.insert(name, attachement);
      }

      //#########################################
      //######### SERVICE MESSAGE ###############
      //#########################################

      ServiceMessage::ServiceMessage(const Message & baseMessage)
         : Message(baseMessage)
      {
      }

      ServiceMessage::ServiceMessage(const QStringList & receivers, const QString & sender, const QString & serviceType, const QString & replyToEndpoint, const QString & replyingToMessage)
         : Message(receivers, replyToEndpoint, sender, replyingToMessage)
         , m_serviceType(serviceType)
      {
      }

      json ServiceMessage::toJson() const
      {
         auto obj = Message::toJson();
         obj["messageType"] = "serviceMessage";
         obj["serviceType"] = m_serviceType.toUtf8();
         return obj;
      }

      ServiceMessage ServiceMessage::fromJson(const json& json)
      {
         auto baseMessage = Message::fromJson(json);
         ServiceMessage serviceMessage(baseMessage);
         if (json.contains("serviceType"))
         {
            auto val = json["serviceType"];
            if (val.is_string())
            {
               serviceMessage.m_serviceType = val.get<std::string>().c_str();
            }
         }
         return serviceMessage;
      }

      ServiceRequest::ServiceRequest(const QStringList & receivers, const QString & sender, const QString & serviceType, json parameters, const QString & replyToEndpoint, const QString & replyingToMessage)
         : ServiceMessage(receivers, sender, serviceType, replyToEndpoint, replyingToMessage)
         , m_parameters(parameters)
      {
      }

      ServiceRequest::ServiceRequest(const ServiceMessage & baseMessage)
         : ServiceMessage(baseMessage)
      {
      }

      ServiceRequest ServiceRequest::fromJson(const json& json)
      {
         auto serviceMessage = ServiceMessage::fromJson(json);
         ServiceRequest serviceRequest(serviceMessage);

         if (json.contains("parameters"))
         {
            auto val = json["parameters"];
            if (val.is_object())
            {
               serviceRequest.m_parameters = val;
            }
         }
         return serviceRequest;
         throw SchemaException("");
      }

      json ServiceRequest::toJson() const
      {
         auto obj = ServiceMessage::toJson();
         obj["messageType"] = "serviceRequest";
         obj["parameters"] = m_parameters;
         return obj;
      }

      ServiceReply::ServiceReply(const QStringList & receivers, const QString & sender, const QString & serviceType, json results, const QString & replyToEndpoint, const QString & replyingToMessage)
         : ServiceMessage(receivers, sender, serviceType, replyToEndpoint, replyingToMessage)
         , m_results(results)
      {
      }

      ServiceReply::ServiceReply(const ServiceMessage & baseMessage)
         : ServiceMessage(baseMessage)
      {
      }

      json ServiceReply::toJson() const
      {
         auto obj = ServiceMessage::toJson();
         obj["messageType"] = "serviceReply";
         obj["results"] = m_results;
         return json(obj);
      }

      ServiceReply ServiceReply::fromJson(const json& json)
      {
         auto serviceMessage = ServiceMessage::fromJson(json);
         ServiceReply serviceReply(serviceMessage);

         if (!json.contains("results"))
            throw SchemaException("ServiceReply contains no 'result' field of type object");
         serviceReply.m_results = json["results"];

         return serviceReply;
      }


      ServiceRequestStatus::ServiceRequestStatus(const QStringList& receivers, const QString& sender, const QString& serviceType, const QString& replyingToMessage, json statusInformation)
         : ServiceMessage(receivers, sender, serviceType, QString(), replyingToMessage)
         , m_statusInformation(statusInformation)
      {
      }

      ServiceRequestStatus::ServiceRequestStatus(const ServiceMessage & baseMessage)
         : ServiceMessage(baseMessage)
      {
      }

      json ServiceRequestStatus::toJson() const
      {
         auto obj = ServiceMessage::toJson();
         obj["messageType"] = "serviceRequestStatus";
         obj["statusInformation"] = m_statusInformation;
         return obj;
      }

      ServiceRequestStatus ServiceRequestStatus::fromJson(const json& json)
      {
         auto serviceMessage = ServiceMessage::fromJson(json);
         ServiceRequestStatus serviceReply(serviceMessage);

         if (json.contains("statusInformation"))
         {
            auto val = json["statusInformation"];
            if (val.is_object())
            {
               serviceReply.m_statusInformation = val;
            }
         }

         return serviceReply;
      }


      //#########################################
      //###### ATTRIBUTE VALUE MESSAGE ##########
      //#########################################



      AttributeValueMessage AttributeValueMessage::fromJson(const json& json)
      {
         auto baseMessage = Message::fromJson(json);
         {
            AttributeValueMessage attributeValueMessage(baseMessage);
            return attributeValueMessage;
         }
         throw SchemaException("");
      }

      AttributeValueMessage::AttributeValueMessage(const Message & baseMessage)
         : Message(baseMessage)
      {
      }

      json AttributeValueMessage::toJson() const
      {
         auto obj = Message::toJson();
         obj["messageType"] = "attributeValueMessage";
         return obj;
      }

      GetValueRequest GetValueRequest::fromJson(const json & json)
      {
         auto baseMessage = GetValueMessage::fromJson(json);
         {
            GetValueRequest getValueRequest(baseMessage);

            if (!json.contains("attributePath"))
               throw SchemaException("");

            getValueRequest.m_attributePath = json["attributePath"].get<std::string>().c_str();
            return getValueRequest;
         }
         throw SchemaException("");
      }

      GetValueRequest::GetValueRequest(const GetValueMessage & baseMessage)
         : GetValueMessage(baseMessage)
      {
      }

      json GetValueRequest::toJson() const
      {
         auto obj = AttributeValueMessage::toJson();
         obj["messageType"] = "getValueRequest";
         obj["attributePath"] = m_attributePath.toUtf8();
         return obj;
      }

      GetValueReply GetValueReply::fromJson(const json & json)
      {
         auto baseMessage = GetValueMessage::fromJson(json);
         {
            GetValueReply getValueReply(baseMessage);

            if (!json.contains("value"))
               throw SchemaException("");

            getValueReply.m_value = json["value"];
            return getValueReply;
         }
         throw SchemaException("");
      }

      GetValueReply::GetValueReply(const GetValueMessage & baseMessage)
         : GetValueMessage(baseMessage)
      {
      }

      json GetValueReply::toJson() const
      {
         auto obj = GetValueMessage::toJson();
         obj["messageType"] = "getValueReply";
         obj["value"] = m_value;
         return obj;
      }

      SetValueRequest SetValueRequest::fromJson(const json & json)
      {
         auto baseMessage = SetValueMessage::fromJson(json);
         {
            SetValueRequest setValueRequest(baseMessage);

            if (!json.contains("attributePath") || !json.contains("newValue"))
               throw SchemaException("");

            setValueRequest.m_attributePath = json["attributePath"].get<std::string>().c_str();
            setValueRequest.m_newValue = json["newValue"];
            return setValueRequest;
         }
         throw SchemaException("");
      }

      SetValueRequest::SetValueRequest(const SetValueMessage & baseMessage)
         : SetValueMessage(baseMessage)
      {
      }

      json SetValueRequest::toJson() const
      {
         auto obj = SetValueMessage::toJson();
         obj["messageType"] = "setValueRequest";
         obj["attributePath"] = m_attributePath.toUtf8();
         obj["newValue"] = m_newValue;
         return obj;
      }

      SetValueReply SetValueReply::fromJson(const json & json)
      {
         auto baseMessage = SetValueMessage::fromJson(json);
         {
            SetValueReply setValueReply(baseMessage);

            if (!json.contains("ok"))
               throw SchemaException("");

            setValueReply.m_ok = json["ok"].get<bool>();
            return setValueReply;
         }
         throw SchemaException("");
      }

      SetValueReply::SetValueReply(const SetValueMessage & baseMessage)
         : SetValueMessage(baseMessage)
      {
      }

      json SetValueReply::toJson() const
      {
         auto obj = SetValueMessage::toJson();
         obj["messageType"] = "setValueReply";
         obj["ok"] = m_ok;
         return obj;
      }

      DeleteAttributeRequest DeleteAttributeRequest::fromJson(const json & json)
      {
         auto baseMessage = Message::fromJson(json);
         {
            DeleteAttributeRequest deleteAttributeRequest(baseMessage);

            if (!json.contains("attributePath"))
               throw SchemaException("");

            deleteAttributeRequest.m_attributePath = json["attributePath"].get<std::string>().c_str();
            return deleteAttributeRequest;
         }
         throw SchemaException("");
      }

      DeleteAttributeRequest::DeleteAttributeRequest(const DeleteAttributeMessage & baseMessage)
         : DeleteAttributeMessage(baseMessage)
      {
      }

      json DeleteAttributeRequest::toJson() const
      {
         auto obj = DeleteAttributeMessage::toJson();
         obj["messageType"] = "deleteAttributeRequest";
         obj["attributePath"] = m_attributePath.toUtf8();
         return obj;
      }

      DeleteAttributeReply DeleteAttributeReply::fromJson(const json & json)
      {
         auto baseMessage = Message::fromJson(json);
         {
            DeleteAttributeReply deleteAttributeReply(baseMessage);

            if (!json.contains("ok"))
               throw SchemaException("");

            deleteAttributeReply.m_ok = json["ok"].get<bool>();
            return deleteAttributeReply;
         }
         throw SchemaException("");
      }

      DeleteAttributeReply::DeleteAttributeReply(const DeleteAttributeMessage & baseMessage)
         : DeleteAttributeMessage(baseMessage)
      {
      }

      json DeleteAttributeReply::toJson() const
      {
         auto obj = DeleteAttributeMessage::toJson();
         obj["messageType"] = "deleteAttributeReply";
         obj["ok"] = m_ok;
         return obj;
      }

      CreateAttributeRequest CreateAttributeRequest::fromJson(const json & json)
      {
         auto baseMessage = Message::fromJson(json);
         {
            CreateAttributeRequest createAttributeRequest(baseMessage);

            if (!json.contains("attributePath") || !json.contains("newValue"))
               throw SchemaException("");

            createAttributeRequest.m_attributePath = json["attributePath"].get<std::string>().c_str();
            createAttributeRequest.m_newValue = json["newValue"];
            return createAttributeRequest;
         }
         throw SchemaException("");
      }

      CreateAttributeRequest::CreateAttributeRequest(const CreateAttributeMessage & baseMessage)
         : CreateAttributeMessage(baseMessage)
      {
      }

      json CreateAttributeRequest::toJson() const
      {
         auto obj = CreateAttributeMessage::toJson();
         obj["messageType"] = "createAttributeRequest";
         obj["attributePath"] = m_attributePath.toUtf8();
         obj["newValue"] = m_newValue;
         return obj;
      }

      CreateAttributeReply CreateAttributeReply::fromJson(const json & json)
      {
         auto baseMessage = Message::fromJson(json);

         CreateAttributeReply createAttributeReply(baseMessage);

         if (!json.contains("ok"))
            throw SchemaException("");

         createAttributeReply.m_ok = json["ok"].get<bool>();
         return createAttributeReply;
         throw SchemaException("");
      }

      CreateAttributeReply::CreateAttributeReply(const CreateAttributeMessage & baseMessage)
         : CreateAttributeMessage(baseMessage)
      {
      }

      json CreateAttributeReply::toJson() const
      {
         auto obj = CreateAttributeMessage::toJson();
         obj["messageType"] = "createAttributeReply";
         obj["ok"] = m_ok;
         return obj;
      }
      GetValueMessage::GetValueMessage(const AttributeValueMessage & baseMessage)
         : AttributeValueMessage(baseMessage)
      {
      }
      SetValueMessage::SetValueMessage(const AttributeValueMessage & baseMessage)
         : AttributeValueMessage(baseMessage)
      {
      }
      DeleteAttributeMessage::DeleteAttributeMessage(const Message & baseMessage)
         : Message(baseMessage)
      {
      }
      CreateAttributeMessage::CreateAttributeMessage(const Message & baseMessage)
         : Message(baseMessage)
      {
      }
      EventMessage::EventMessage(
         const QString& sender, 
         const QString& topic, 
         const QDateTime& timestamp, 
         json content)
         : Message({}, "", sender)
         , topic(topic)
         , timestamp(timestamp)
         , content(content)
      {
      }

      EventMessage::EventMessage(const Message& baseMessage)
         : Message(baseMessage)
      {
      }

      json EventMessage::toJson() const
      {
         auto obj = Message::toJson();
         obj["messageType"] = "eventMessage";
         obj["topic"] = topic.toStdString();
         obj["content"] = content;
         obj["timestamp"] = timestamp.toMSecsSinceEpoch();
         return obj;
      }

      EventMessage EventMessage::fromJson(const json& json)
      {
         EventMessage evt(Message::fromJson(json));

         if (!(json.contains("topic") && json.contains("timestamp") && json.contains("content")))
            throw SchemaException("");

         evt.content = json["content"];
         evt.topic = json["topic"].get<std::string>().c_str();
         evt.timestamp = QDateTime::fromMSecsSinceEpoch(json["timestamp"].get<int>());
            
         return evt;
      }

      CustomEventRequest::CustomEventRequest(
         const QStringList& receivers,
         const QString& replyToEndpoint,
         const QString& sender, 
         const QString& filter, 
         const QStringList& attributePaths)
         : Message(receivers, replyToEndpoint, sender)
         , filter(filter)
         , attributePaths(attributePaths)
      {
      }

      CustomEventRequest::CustomEventRequest(const Message& baseMessage)
         : Message(baseMessage)
      {
      }

      json CustomEventRequest::toJson() const
      {
         auto obj = Message::toJson();
         obj["messageType"] = "customEventRequest";
         obj["filter"] = filter.toStdString();

         for (const QString attributePath : attributePaths)
         {
            obj["attributePaths"].push_back(attributePath.toUtf8());
         }
         return obj;
      }

      CustomEventRequest CustomEventRequest::fromJson(const json& json)
      {
         CustomEventRequest evt(Message::fromJson(json));

         if (!(json.contains("filter") && json.contains("attributePaths")))
            throw SchemaException("");

         evt.filter = json["filter"].get<std::string>().c_str();
         auto attributePaths = json["attributePaths"];
         if (attributePaths.is_array())
         {
            for (auto entry : attributePaths)
            {
               if (entry.is_string())
                  evt.attributePaths<< entry.get<std::string>().c_str();
            }
         }

         return evt;
      }
      
      CustomEventReply::CustomEventReply(const QStringList& receivers, const QString& sender, const QString& status, const QString& topic)
         : Message(receivers, "", sender)
         , status(status)
         , topic(topic)
      {
      }
      
      CustomEventReply::CustomEventReply(const Message& baseMessage)
         : Message(baseMessage)
      {
      }
      
      json CustomEventReply::toJson() const
      {
         auto obj = Message::toJson();
         obj["messageType"] = "customEventReply";
         obj["status"] = status.toStdString();
         obj["topic"] = topic.toStdString();
         return obj;
      }
      
      CustomEventReply CustomEventReply::fromJson(const json& json)
      {
         CustomEventReply evt(Message::fromJson(json));

         if (!(json.contains("status") && json.contains("topic")))
            throw SchemaException("");

         evt.status = json["status"].get<std::string>().c_str();
         evt.topic = json["topic"].get<std::string>().c_str();

         return evt;
      }
}
}