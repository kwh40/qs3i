#pragma once

#include "qs3i-global.h"
#include "brokerMessage.h"

#include <QObject>
#include <memory>

namespace S3I
{
   class S3IManager;
   namespace Broker
   {
      class BaseInterface : public QObject
      {
         Q_OBJECT;

      public:
         virtual void sendDirectMessage(std::shared_ptr<Message> msg, const QStringList& endpoints) = 0;
         virtual void publishEvent(const std::shared_ptr<EventMessage>&, const QString&) = 0;

      };

      class PassiveInterface : public BaseInterface
      {
         Q_OBJECT;
      public:
         virtual Message getMessage() = 0;
      };

      class QS3I_DECLSPEC ActiveInterface : public BaseInterface
      {
         Q_OBJECT;
      public:
         ActiveInterface(S3IManager* s3i);
         virtual void startConsuming(const QString& endpoint) = 0;
         virtual void stopConsuming(const QString& endpoint) = 0;
         
      protected:
         S3IManager* m_s3i;

      protected slots:
         void messageReceived(const QString& msg);

      signals:
         void signalInvalidMessageReceived(const QString& payload, const QString& error);
         void signalUserMessageReceived(const S3I::Broker::UserMessage& msg);
         void signalServiceReplyReceived(const S3I::Broker::ServiceReply& reply);
         void signalGetValueRequestReceived(const S3I::Broker::GetValueRequest& req);
         void signalGetValueReplyReceived(const S3I::Broker::GetValueReply& reply);
         void signalSetValueRequestReceived(const S3I::Broker::SetValueRequest& req);
         void signalSetValueReplyReceived(const S3I::Broker::SetValueReply& reply);
         void signalCreateAttributeReplyReceived(const S3I::Broker::CreateAttributeReply& reply);
         void signalDeleteAttributeReplyReceived(const S3I::Broker::DeleteAttributeReply& reply);
         void signalServiceRequestReceived(const S3I::Broker::ServiceRequest& request);
         void signalServiceRequestStatusReceived(const S3I::Broker::ServiceRequestStatus& request);
         void signalEventMessageReceived(const S3I::Broker::EventMessage& evt);
         void signalCustomEventRequestReceived(const S3I::Broker::CustomEventRequest& evt);
         void signalCustomEventReplyReceived(const S3I::Broker::CustomEventReply& evt);
      };
   }
}