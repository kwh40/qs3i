#ifndef UTILS_H
#define UTILS_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonArray>

namespace S3I
{
   namespace Utils
   {
      // Json
      template<typename T> QJsonArray jsonArrayFromIterable(T iterable);
      bool jsonObjectPathExists(const QJsonObject& obj, const QStringList& pathElements);
      bool jsonObjectHasStringFields(const QJsonObject& obj, const QStringList& fields);

      // Network
      QString appendUrlParam(QString url, const QString& name, const QStringList& args);
      QNetworkReply* waitForReplyAvailable(QNetworkReply* reply);

      QString urlencode(const QString& str);
      QString x_www_form_urlencode(const QHash<QString, QString>& parameters);

      QString getFilenameFromPath(const QString& path);
   };

#include "utils.hpp"
}
#endif // UTILS_H
