QT -= gui 
QT += network

TEMPLATE = lib
TARGET = s3i-cpp-qt
DESTDIR = bin
DLLDESTDIR = ../../s3i-cli-qt/build-s3i-cli-qt-Desktop_Qt_5_15_0_MSVC2019_64bit-Debug/bin
OBJECTS_DIR = tmp
DEFINES += S3ICPPQT_LIBRARY

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS \
            S3ICPP_EXPORT


# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    endpoint.cpp \
    entry.cpp \
    error.cpp \
    query.cpp \
    queryresult.cpp \
    policyEntry.cpp \
    policyGroup.cpp \
    policySubject.cpp \
    policyResource.cpp \
    result.cpp \
    role.cpp \
    rqlQuery.cpp \
    s3i.cpp  \
    service.cpp \
    thing.cpp \
    token.cpp \
    utils.cpp

HEADERS += \
    endpoint.h \
    entry.h \
    error.h \
    query.h \
    queryresult.h \
    src/policyEntry.h \
    src/policyGroup.h \
    src/policySubject.h \
    src/policyResource.h \
    result.h \
    role.h \
    rqlQuery.h \
    s3i-cpp-qt_global.h \
    s3i.h  \
    service.h \
    thing.h \
    token.h \
    utils.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target



