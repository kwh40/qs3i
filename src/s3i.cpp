#include "s3i.h"
#include "utils.h"
#include "policyEntry.h"
#include "exception.h"
#include "query.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QEventLoop>
#include <QObject>
#include <QThread>

static const QString CONFIG_API_BASE_URL("https://config.s3i.vswf.dev/");

namespace S3I
{
   

   S3IManager::S3IManager(unique_ptr<AuthenticationManager> authManager)
      : m_authManager(std::move(authManager))
      , m_qnam()
      , m_directoryUrl("https://dir.s3i.vswf.dev/api/2/")
      , m_repositoryUrl("https://ditto.s3i.vswf.dev/api/2/")
   {
   }

   void S3IManager::login(RefreshPolicy policy)
   {
      Q_UNUSED(policy)

      m_authManager->getAccessToken(5);

      // RefreshPolicy setup/handling can be done here

      return;
   }

   void S3IManager::logout()
   {
      m_authManager->invalidateSession();
      m_authManager->resetTokens();
   }


   Directory::Thing S3IManager::getThingById(const QString & id)
   {
      QNetworkRequest req;
      
      setTokenHeader(req);
      //   if (!setTokenHeader(req))
      //      return Directory::Thing();
      req.setUrl(m_directoryUrl + QString("things/") + id);
      req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
      auto reply = awaitGet(req);

      auto replyData = reply->readAll();
      // qDebug() << "Reply: " << replyData;
      auto responseObj = QJsonDocument::fromJson(replyData).object();

      return Directory::Thing::fromJson(this, responseObj);
   }

   json S3IManager::getThingFieldById(const QString & id, const QString & field)
   {
      try
      {
         const json::json_pointer jsonPointer(field.toStdString());
         return getThingFieldsById(id, { field }).at(jsonPointer);
      }
      catch (json::exception e) {
         throw ParseException(e.what());
      }
   }

   json S3IManager::getThingFieldsById(const QString & id, const QStringList & fields)
   {
      QNetworkRequest req;
      // This is shit, we need good error types
      setTokenHeader(req);
      //   if (!setTokenHeader(req))
      //      return Directory::Thing();
      auto url = m_directoryUrl + QString("things/") + id + "?";
      url = Utils::appendUrlParam(url, "fields", fields);
      req.setUrl(url);
      req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
      auto reply = awaitGet(req);

      auto replyData = reply->readAll();
      // qDebug() << "Reply: " << replyData;
      json responseJson;
      try
      {
         responseJson = json::parse(replyData.toStdString());
      }
      catch (json::exception e)
      {
         throw InvalidJsonException(e.what());
      }
      if (responseJson.is_null())
         throw InvalidJsonException("Response Document was null");
      
      return responseJson;
   }

   Policy::PolicyEntry S3IManager::getPolicyById(const QString & id)
   {
      QNetworkRequest req;
      // This is shit, we need good error types
      setTokenHeader(req);
      //   if (!setTokenHeader(req))
      //      return Directory::Thing();

      req.setUrl(m_directoryUrl + QString("policies/") + id);
      req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
      auto reply = awaitGet(req);

      auto replyData = reply->readAll();
      // qDebug() << "Reply: " << replyData;
      auto responseObj = QJsonDocument::fromJson(replyData).object();

      return Policy::PolicyEntry::fromJson(this, responseObj, Policy::PolicyFor::Directory);
   }

   QString S3IManager::getOwnerThingId(const QString & ownedThingId)
   {
      QString userKeycloakId;
      try
      {
         userKeycloakId = QString::fromStdString(getThingFieldById(ownedThingId, "/attributes/ownedBy"));
      }
      catch (json::exception e)
      {
         throw ParseException(e.what());
      }

      const auto queriedThings = Query::create()
         .filter(RQL::eq("/attributes/represents", userKeycloakId))
         .fields({ "thingId" })
         .execute(this);

      if (queriedThings.length() > 1)
         throw Exception("Ambiguous user representation");

      if (queriedThings.isEmpty())
         throw Exception("Missing user representation");

      return queriedThings.first().identifier;
   }

   Directory::Thing S3IManager::getOwnerThing(const Directory::Thing& ownedThing)
   {
      const auto userKeycloakId = ownedThing.ownedBy;
      const auto queriedThings = Query::create()
         .filter(RQL::eq("/attributes/represents", userKeycloakId))
         .execute(this);

      if (queriedThings.length() > 1)
         throw Exception("Ambiguous user representation");

      if (queriedThings.isEmpty())
         throw Exception("Missing user representation");

      return queriedThings.first();
   }

   QList<Directory::Thing> S3IManager::executeQuery(const QString& query)
   {
      QString queryUrl(m_directoryUrl + query);
      qDebug() << "Executing Query: " << queryUrl;

      QNetworkRequest req;
      // This is shit, we need good error types
      if (!setTokenHeader(req))
         return QList<Directory::Thing>();

      req.setUrl(queryUrl);
      req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
      auto reply = awaitGet(req);

      auto replyData = reply->readAll();
      //qDebug() << "Reply: " << replyData;
      auto responseObj = QJsonDocument::fromJson(replyData).object();


      // Thing from JSON
      QList<Directory::Thing> things;
      for (auto item : responseObj["items"].toArray())
      {
         try {
            things.append(Directory::Thing::fromJson(this, item.toObject()));
         }
         catch (ParseException e)
         {
            qDebug() << e.what();
         }
      }

      return things;
   }

   QString S3IManager::getDirectoryUrl() const
   {
      return m_directoryUrl;
   }

   QString S3IManager::getRepositoryUrl() const
   {
      return m_repositoryUrl;
   }

   AuthenticationManager * S3IManager::getAuthManager() const
   {
      return m_authManager.get();
   }

   void S3IManager::setAuthManager(unique_ptr<AuthenticationManager> authManager)
   {
      m_authManager = std::move(authManager);
   }

   QPair<QString, QString> S3IManager::createThing(const ClientRepresentation& clientRepr)
   {
      QPair<QString, QString> ret;
      // S3I Management API call
      // - 

      QNetworkRequest req;
      req.setUrl(QString(CONFIG_API_BASE_URL + "things/"));
      req.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/json");
      setTokenHeader(req);

      const auto body = QJsonDocument(clientRepr.toJsonObject()).toJson();

      const auto reply = Utils::waitForReplyAvailable(m_qnam.post(req, body));

      if (reply->error() != QNetworkReply::NoError)
         throw NetworkException(reply);

      const auto replyJsonDoc = QJsonDocument::fromJson(reply->readAll());
      if (!replyJsonDoc.isObject())
      {
         qDebug() << "S3IManager::createThing -> Response body was not a Json Object: " << replyJsonDoc.toJson();
         throw InvalidJsonException("Response body was not a Json Object");
      }

      const auto replyJsonObj = replyJsonDoc.object();

      if (!replyJsonObj.contains("identifier") || !replyJsonObj.contains("secret"))
      {
         qDebug() << "S3IManager::createThing -> Response body did not contain identifier and secret: " << replyJsonDoc.toJson();
         throw SchemaException("Response body did not contain identifier and secret");
      }

      ret.first = replyJsonObj["identifier"].toString();
      ret.second = replyJsonObj["secret"].toString();

      return ret;

   }

   void S3IManager::deleteThing(const QString & thingId)
   {
      QNetworkRequest req;
      req.setUrl(CONFIG_API_BASE_URL + "things/" + thingId);
      req.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/json");
      setTokenHeader(req);

      const auto reply = Utils::waitForReplyAvailable(m_qnam.deleteResource(req));

      if (reply->error() != QNetworkReply::NoError)
      {
         qDebug() << "S3IManager::deleteThing-> DELETE returned with error: " << reply->errorString();
         throw NetworkException(reply);
      }
   }

   QString S3IManager::createBrokerEndpoint(const QString & thingId, bool encrypted)
   {
      QNetworkRequest req;

      req.setUrl(CONFIG_API_BASE_URL + "things/" + thingId + "/broker");

      req.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/json");
      setTokenHeader(req);

      QJsonObject obj;
      obj["encrypted"] = encrypted;

      const auto body = QJsonDocument(obj).toJson();

      const auto reply = Utils::waitForReplyAvailable(m_qnam.post(req, body));

      if (reply->error() != QNetworkReply::NoError)
         throw NetworkException(reply);

      const auto replyJsonDoc = QJsonDocument::fromJson(reply->readAll());
      if (!replyJsonDoc.isObject())
         throw InvalidJsonException(QString("Response body was not a Json Object: " + QString(replyJsonDoc.toJson())));

      const auto replyJsonObj = replyJsonDoc.object();

      if (!replyJsonObj.contains("queue_name") || !replyJsonObj["queue_name"].isString())
         throw SchemaException("Server reply contains no field 'queue_name' of type string");
         
      return replyJsonObj["queue_name"].toString();
   }

   bool S3IManager::setTokenHeader(QNetworkRequest & req)
   {
      QString token_header = "Bearer " + m_authManager->getAccessToken(5).encoded();
      req.setRawHeader(QByteArray("Authorization"), token_header.toUtf8());
      return true;
   }

   QNetworkReply * S3IManager::awaitGet(const QNetworkRequest & req)
   {
      const auto reply = Utils::waitForReplyAvailable(m_qnam.get(req));
      if (reply->error() != QNetworkReply::NoError)
         throw NetworkException(reply);
      return reply;
   }

   QNetworkReply * S3IManager::awaitPut(const QNetworkRequest & req, const QByteArray & data)
   {
      const auto reply = Utils::waitForReplyAvailable(m_qnam.put(req, data));
      if (reply->error() != QNetworkReply::NoError)
         throw NetworkException(reply);
      return reply;
   }
}