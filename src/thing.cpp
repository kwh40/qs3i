#include "thing.h"
#include "s3i.h"
#include "utils.h"
#include "exception.h"
#include <QJsonDocument>
#include <qnetworkreply.h>
namespace S3I
{
   namespace Directory {

      //Thing::operator QString() const
      //{
      //    return QJsonDocument(toJson()).toJson();
      //}

      Thing Thing::fromJson(S3IManager * s3i, const QJsonObject & jsonThing)
      {
         Thing thing(s3i, jsonThing["thingId"].toString());

         if (jsonThing.contains("attributes"))
         {
            const auto attrObj = jsonThing["attributes"].toObject();

            thing.dataModel = attrObj["dataModel"].toString();
            thing.name = attrObj["name"].toString();
            thing.ownedBy = attrObj["ownedBy"].toString();
            thing.administratedBy = attrObj["administratedBy"].toString();
            thing.usedBy = attrObj["usedBy"].toString();
            thing.represents = attrObj["represents"].toString();
            thing.location = attrObj["location"].toString();
            thing.defaultHMI = attrObj["defaultHMI"].toString();
            thing.publicKey = attrObj["publicKey"].toString().toUtf8();
            thing.thingType = thingTypeFromString(attrObj["thingType"].toString());
            thing.defaultEndpoint = attrObj["defaultEndpoint"].toString();

            if (attrObj.contains("thingStructure"))
               thing.thingStructure = DirObject::fromJson(attrObj["thingStructure"].toObject());
            
            if (attrObj.contains("thingStructure"))
            {
               const auto allEPs = attrObj["allEndpoints"].toArray();

               for (const auto ep : allEPs)
               {
                  thing.allEndpoints.append(Endpoint(ep.toString()));
               }
            }
         }


         return thing;
      }

      
      Thing & Thing::operator=(const Thing & other)
      {
         // TODO: hier R�ckgabeanweisung eingeben
         m_s3i = other.m_s3i;
         dataModel = other.dataModel;
         location = other.location;
         name = other.name;
         defaultHMI = other.defaultHMI;
         ownedBy = other.ownedBy;
         administratedBy = other.administratedBy;
         usedBy = other.usedBy;
         represents = other.represents;

         publicKey = other.publicKey;
         thingType = other.thingType;
         allEndpoints = other.allEndpoints;
         defaultEndpoint = other.defaultEndpoint;
         thingStructure = other.thingStructure;

         return *this;
      }

      void Thing::commit() const
      {
         // Put request to /things/{thingId}
         QNetworkRequest req;
         m_s3i->setTokenHeader(req);
         req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
         req.setUrl(m_s3i->getDirectoryUrl() + QString("things/%1").arg(identifier));
         m_s3i->awaitPut(req, QJsonDocument(toJson()).toJson());
      }

      QJsonObject Thing::toJson() const
      {
         QJsonObject obj;
         QJsonObject attr;

         if (!identifier.isNull())
            obj["identifier"] = identifier;
         // This field is required by our ditto schema
         if (!identifier.isNull())
            obj["policyId"] = identifier;
         if (!dataModel.isNull())
            attr["dataModel"] = dataModel;
         if (!defaultHMI.isNull())
            attr["defaultHMI"] = defaultHMI;
         if (!location.isNull())
            attr["location"] = location;
         if (!name.isNull())
            attr["name"] = name;
         if (!administratedBy.isNull())
            attr["administratedBy"] = administratedBy;
         if (!ownedBy.isNull())
            attr["ownedBy"] = ownedBy;
         if (!usedBy.isNull())
            attr["usedBy"] = usedBy;
         if (!represents.isNull())
            attr["represents"] = represents;
         if (!publicKey.isNull())
            attr["publicKey"] = QString::fromUtf8(publicKey);
         if (thingType != ThingType::Undefined)
            attr["thingType"] = stringFromThingType(thingType);
         if (!defaultEndpoint.m_uri.isNull())
            attr["defaultEndpoint"] = defaultEndpoint.m_uri;
         if (thingStructure.isValid())
            attr["thingStructure"] = thingStructure.toJson();
         if (!allEndpoints.isEmpty())
            attr["allEndpoints"] = Utils::jsonArrayFromIterable(allEndpoints);

         obj["attributes"] = attr;

         return obj;
      }


      void Thing::updateFromJson(const QJsonObject & obj)
      {
      }

      Thing::Thing(S3IManager* s3i, const QString& id)
         : Entry(s3i, id)
      {

      }

      ThingType thingTypeFromString(const QString & thingType)
      {
         if (thingType == QString("component"))
            return ThingType::Component;
         if (thingType == QString("service"))
            return ThingType::Service;
         if (thingType == QString("hmi"))
            return ThingType::Hmi;
         return ThingType::Undefined;
      }

      QString stringFromThingType(const ThingType& thingType)
      {
         if (thingType == ThingType::Component)
            return QString("component");
         if (thingType == ThingType::Hmi)
            return QString("hmi");
         if (thingType == ThingType::Service)
            return QString("service");
         if (thingType == ThingType::Undefined)
            return QString(); // Or should it say undefined?

         return QString();
      }

      DirObject DirObject::fromJson(const QJsonObject & obj)
      {
         if (!Utils::jsonObjectHasStringFields(obj, { "class" }))
            throw InvalidJsonException("Missing key: 'class' in S3I:Dir:Object");

         DirObject dirObj;
         dirObj.clazz = obj["class"].toString();
         if (obj.contains("identifier") && obj["identifier"].isString())
            dirObj.identifier = obj["identifier"].toString();
         if (obj.contains("links"))
         {
            if (!obj["links"].isArray())
               throw SchemaException("links not an array");

            for (const auto jsonLink : obj["links"].toArray())
            {
               if (!jsonLink.isObject())
                  throw SchemaException("link not an object");
               dirObj.links.append(DirLink::fromJson(jsonLink.toObject()));
            }
         }
         if (obj.contains("values") )
         {
            if (!obj["values"].isArray())
               throw SchemaException("Values not an Array");

            for (const auto jsonValue : obj["values"].toArray())
            {
               if (!jsonValue.isObject())
                  throw SchemaException("value not an object");
               dirObj.values.append(DirValue::fromJson(jsonValue.toObject()));
            }
         }

         return dirObj;
      }

      QJsonObject DirObject::toJson() const
      {
         QJsonObject obj;

         if (!identifier.isNull())
            obj["identifier"] = identifier;
         if (clazz.isNull())
            throw SchemaException("Mandatory field 'class' is null");
         obj["class"] = clazz;
         if (!links.isEmpty()) 
            obj["links"] = Utils::jsonArrayFromIterable(links);
         if (!values.isEmpty())
            obj["values"] = Utils::jsonArrayFromIterable(values);
         return obj;
      }

      DirObject::operator QJsonValue() const
      {
         return toJson();
      }

      QList<DirObject*> Thing::getFeaturesByClassName(const QString & className) const noexcept
      {
         QList<DirObject*> features;
         for (const auto link : thingStructure.links)
         {
            if (link.association == "features" && link.target->clazz == className)
            {
               features.append(link.target.get());
            }
         }
         return features;
      }

      QStringList& Thing::getFirstFeatureValueMatchingClassAndAttribute(const QString & clazz, const QString& attribute) const
      {
         const auto features = getFeaturesByClassName(clazz);

         for (const auto feature : features)
         {
            for (DirValue& value : feature->values)
            {
               if (value.attribute == attribute)
               {
                  return value.value;
               }
            }
         }

         throw KeyException(QString("No feature with class '%1' and attribute '%2'").arg(clazz, attribute));
      }

      QStringList & Thing::getOrCreateFirstFeatureValueMatchingClassAndAttribute(const QString & clazz, const QString & attribute) noexcept
      {
         const auto features = getFeaturesByClassName(clazz);
         // check if any matches both class and attribute
         for (const auto feature : features)
         {
            for (DirValue& value : feature->values)
            {
               if (value.attribute == attribute)
               {
                  return value.value;
               }
            }
         }

         if (features.isEmpty())
         {
            auto newObject = std::make_shared<DirObject>();
            newObject->clazz = clazz;

            DirLink newFeature;
            newFeature.association = "features";
            newFeature.target = newObject;

            thingStructure.links.append(newFeature);
         }

         // None matched -> create attribute in the first feature matching clazz
         const auto feature = getFeaturesByClassName(clazz).first();
         DirValue value;
         value.attribute = attribute;
         feature->values.append(value);

         // this call must return the newly created value
         return getOrCreateFirstFeatureValueMatchingClassAndAttribute(clazz, attribute);
      }

      bool DirObject::isValid() const noexcept
      {
         // clazz is a mandatory field. Everything else is optional
         return !clazz.isNull();
      }

      DirLink DirLink::fromJson(const QJsonObject & obj)
      {
         if (!Utils::jsonObjectHasStringFields(obj, { "association" }))
            throw InvalidJsonException("Missing key: 'association' in S3I:Dir:Link");
         
         DirLink link;
         link.association = obj["association"].toString();
         if (obj.contains("target") && obj["target"].isObject())
            link.target = std::make_shared<DirObject>(DirObject::fromJson(obj["target"].toObject()));

         return link;
      }

      QJsonObject DirLink::toJson() const
      {
         QJsonObject obj;

         if (association.isNull())
            throw SchemaException("Mandatory field 'association' is null");
         obj["association"] = association;
         
         if (target->isValid())
            obj["target"] = target->toJson();

         return obj;
      }

      DirLink::operator QJsonValue() const
      {
         return toJson();
      }

      DirValue DirValue::fromJson(const QJsonObject & obj)
      {
         if (!Utils::jsonObjectHasStringFields(obj, { "attribute" }))
            throw InvalidJsonException("Missing key: 'attribute' in S3I:Dir:Value");
         
         DirValue val;
         val.attribute = obj["attribute"].toString();
         if (obj.contains("value") && obj["value"].isArray())
         {
            const auto valJsonArray = obj["value"].toArray();
            for (const auto v : valJsonArray) {
               val.value.append(v.toString());
            }
         }

         return val;
      }

      QJsonObject DirValue::toJson() const
      {
         QJsonObject obj;

         if (attribute.isNull())//TODO: warn if Null?
            throw SchemaException("Mandatory field 'attribute' is null");
         obj["attribute"] = attribute;
         if(!value.isEmpty())
            obj["value"] = Utils::jsonArrayFromIterable(value);

         return obj;
      }

      DirValue::operator QJsonValue() const
      {
         return toJson();
      }

   }
}
