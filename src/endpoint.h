#ifndef ENDPOINT_H
#define ENDPOINT_H
#include "qs3i-global.h"
#include <QString>
#include <QJsonValue>

namespace S3I
{

   namespace Directory {

      class QS3I_DECLSPEC Endpoint
      {
      public:
         Endpoint(const QString& uri = QString());

         operator QJsonValue() const;
         operator QString() const;

         QString m_uri;
      };

   }
}
#endif // ENDPOINT_H
