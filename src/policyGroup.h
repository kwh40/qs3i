#pragma once

#include "policyResource.h"
#include "policySubject.h"

#include <QMap>
#include <QJsonObject>
namespace S3I
{
   namespace Policy
   {
      class Group
      {
      public:
         Group() = default;
         Group(QString name);
         Group(QString name, QMap<QString, Subject>& subjects, QMap<QString, Resource>& resources);
         static Group fromJson(const QString& name, const QJsonObject& obj);

         QJsonObject toJson() const noexcept;

         QString getName() const noexcept;
         QMap<QString, Subject> getSubjects() const noexcept;
         QMap<QString, Resource> getResources() const noexcept;
         Subject getSubject(QString id) const;
         Resource getResource(QString path) const;
         /// append or overwrite
         void insertSubject(const Subject& sub);
         void insertResource(const Resource& res);
         int deleteSubject(QString id) noexcept;
         int deleteResource(QString path) noexcept;

         bool isValid() const noexcept;

      private:
         // name of the policy group
         QString m_name;
         // holds all subjects of this group (key: Subject::id)
         QMap<QString, Subject> m_subjects;
         // holds all resources of this group (key: Resource::path)
         QMap<QString, Resource> m_resources;
      };
   }
}