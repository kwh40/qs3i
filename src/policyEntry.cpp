#include "policyEntry.h"
#include "s3i.h"
#include "exception.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>

namespace S3I
{

   namespace Policy
   {
      PolicyEntry::PolicyEntry(S3IManager* s3i, const QString & identifier, const PolicyFor& target)
         : Entry(s3i, identifier)
         , m_policyFor(target)
      {
      }

      void PolicyEntry::commit() const
      {
         QNetworkRequest req;
         m_s3i->setTokenHeader(req);

         req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
         if (m_policyFor == PolicyFor::Directory)
         {
            req.setUrl(m_s3i->getDirectoryUrl() + QString("policies/%1").arg(identifier));
         }
         else if (m_policyFor == PolicyFor::Repository)
         {
            req.setUrl(m_s3i->getRepositoryUrl() + QString("policies/%1").arg(identifier));
         }

         try
         {
            const QString json = QJsonDocument(toJson()).toJson();
            const auto reply = m_s3i->awaitPut(req, QJsonDocument(toJson()).toJson());
         }
         catch (NetworkException e) {
            qDebug() << e.what();
         }
      }

      void PolicyEntry::pull()
      {
         // Get current state from directory
         return;
      }

      QString PolicyEntry::getETag() const noexcept
      {
         return m_eTag;
      }

      QMap<QString, Group> PolicyEntry::getGroups() const noexcept
      {
         return m_groups;
      }

      Group PolicyEntry::getGroup(QString name) const
      {
         if (!m_groups.contains(name))
            throw std::invalid_argument("Key Error");
         return m_groups[name];
      }

      void PolicyEntry::insertGroup(const Group& group)
      {
         if (!group.isValid())
            throw std::invalid_argument("invalid group");

         m_groups.insert(group.getName(), group);
      }

      int PolicyEntry::deleteGroup(QString name) noexcept
      {
         return m_groups.remove(name);
      }

      void PolicyEntry::insertObserver(const Subject & obs) noexcept
      {
         if (!m_groups.contains("observer"))
         {
            Group observer("observer");
            observer.insertResource(Resource("thing:/", { PermissionType::Read }, {}));
            insertGroup(observer);
         }

         m_groups["observer"].insertSubject(obs);
      }

      QJsonObject PolicyEntry::toJson() const noexcept
      {
         QJsonObject obj;

         obj["policyId"] = identifier;

         QJsonObject entries;

         for (const auto group : m_groups)
         {
            entries[group.getName()] = group.toJson();
         }
         obj["entries"] = entries;

         return obj;
      }

      PolicyEntry PolicyEntry::fromJson(S3IManager* s3i, const QJsonObject & policyObj, const PolicyFor & target)
      {
         if (!policyObj.contains("policyId") || !policyObj["policyId"].isString())
            throw std::invalid_argument("policy object does not contain field 'policyId' of type string");

         PolicyEntry policy(s3i, policyObj["policyId"].toString(), target);

         if (policyObj.contains("entries") && policyObj["entries"].isObject())
         {
            const auto entriesObj = policyObj["entries"].toObject();
            for (const auto groupName : entriesObj.keys())
            {
               try
               {
                  policy.insertGroup(Group::fromJson(groupName, entriesObj[groupName].toObject()));
               }
               catch (std::invalid_argument e) {
                  // do nothing
               }
            }
         }
         return policy;
      }


   }
}