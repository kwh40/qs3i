#pragma once

#include "authenticationManager.h"

namespace S3I
{

   class QS3I_DECLSPEC OAuthProxyFlow : public AuthenticationManager
   {
      Q_OBJECT

   public:
      OAuthProxyFlow(const QString& clientId, const QString& clientSecret, std::function<bool(QString)> openUrlCallback);
      AccessToken getAccessToken(qint64 minimumRemainingLifetimeSeconds = 5) override;

   private:
      QPair<AccessToken, RefreshToken> parseTokensFromJsonResponse(const QJsonDocument& jsonResponse) const;

      const std::function<bool(QString)> m_openUrlCallback;
   };

}