#include "oAuthProxyFlow.h"
#include "utils.h"
#include "exception.h"

#include <QNetworkReply>
#include <QNetworkRequest>
#include <QThread>
#include <QJsonDocument>

namespace S3I
{

   OAuthProxyFlow::OAuthProxyFlow(const QString & clientId, const QString & clientSecret, std::function<bool(QString)> openUrlCallback)
      : AuthenticationManager(clientId, clientSecret)
      , m_openUrlCallback(openUrlCallback)
   {
   }

   AccessToken OAuthProxyFlow::getAccessToken(qint64 minimumRemainingLifetimeSeconds)
   {
      // Checks
      // - Current AccessToken valid
      if (m_accessToken.secsToExpiration() > minimumRemainingLifetimeSeconds)
      {
         return m_accessToken;
      }
      // qDebug() << m_refreshToken.encoded();
      // - Current RefreshToken Valid
      if (m_refreshToken.secsToExpiration() > 5)
      {
         // Token Refresh
         QNetworkRequest refreshReq;
         refreshReq.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "application/x-www-form-urlencoded");
         refreshReq.setUrl(QString("https://idp.s3i.vswf.dev/auth/realms/KWH/protocol/openid-connect/token?"));// TODO make configurable

         auto refreshUrl = S3I::Utils::appendUrlParam(QString(), "grant_type", { "refresh_token" });
         refreshUrl = S3I::Utils::appendUrlParam(refreshUrl, "client_id", { m_clientId });
         refreshUrl = S3I::Utils::appendUrlParam(refreshUrl, "client_secret", { m_clientSecret });
         refreshUrl = S3I::Utils::appendUrlParam(refreshUrl, "refresh_token", { m_refreshToken.encoded() });

         // qDebug() << refreshUrl;
         //QJsonObject refreshBody;
         //refreshBody["grant_type"] = "refresh_token";
         //refreshBody["client_id"] = m_clientId;
         //refreshBody["client_secret"] = m_clientSecret;
         //refreshBody["refresh_token"] = m_refreshToken.encoded();

         const auto refreshReply = Utils::waitForReplyAvailable(m_qnam.post(
            refreshReq,
            refreshUrl.toUtf8()
         ));

         if (refreshReply->error() != QNetworkReply::NetworkError::NoError)
         {
            throw NetworkException(refreshReply);
         }
         else
         {
            const auto replyJson = QJsonDocument::fromJson(refreshReply->readAll());
            if (replyJson.isNull())
               throw S3I::InvalidJsonException(QString(""));

            const auto tokens = parseTokensFromJsonResponse(replyJson);
            m_accessToken = tokens.first;
            m_refreshToken = tokens.second;

            emit signalRefreshTokenUpdated();
            return m_accessToken;
         }
      }

      // Full Flow

      QString base_url("https://auth.s3i.vswf.dev");

      QString init_url(base_url + QString("/initialize/%1/%2").arg(m_clientId, m_clientSecret));
      if (!m_scopes.isEmpty())
         init_url += "/" + m_scopes.join(" ");

      QNetworkRequest initReq(init_url);
      auto initReply = Utils::waitForReplyAvailable(m_qnam.get(initReq));

      if (initReply->error() != QNetworkReply::NoError)
      {
         qDebug() << QString("Initialize request returned with error: %1").arg(initReply->errorString());
         throw S3I::NetworkException(initReply);
      }

      auto initResponseJson = QJsonDocument::fromJson(initReply->readAll());

      qDebug() << initResponseJson;
      assert(initResponseJson.isObject());

      QString authenticate_url = base_url + initResponseJson["redirect_url"].toString();

      if (!m_openUrlCallback(authenticate_url))
         throw Exception("Could not open URL in Browser");

      QString pickup_url = base_url + QString("/pickup/%1/%2")
         .arg(initResponseJson["proxy_user_identifier"].toString(),
            initResponseJson["proxy_secret"].toString());

      QNetworkRequest pickupReq(pickup_url);
      int maxRetries = 100;
      for (int i = 0; i < maxRetries; i++)
      {
         qDebug() << QString("Hitting /pickup endpoint try: %1").arg(i);
         auto pickupReply = Utils::waitForReplyAvailable(m_qnam.get(pickupReq));

         if (pickupReply->error() != QNetworkReply::NoError)
            throw S3I::NetworkException(pickupReply);

         auto jsonResponse = QJsonDocument::fromJson(pickupReply->readAll());

         const auto tokenPair = parseTokensFromJsonResponse(jsonResponse);

         if (tokenPair.first.isEmpty() || tokenPair.second.isEmpty())
         {
            qDebug() << "Received empty string, trying again";
         }
         else {
            // bool decodeOk;
            m_accessToken = tokenPair.first;
            m_refreshToken = tokenPair.second;

            emit signalRefreshTokenUpdated();

            return m_accessToken;
         }
         QThread::sleep(1);
      }

      throw MaxRetryException("Max Retries completed, giving up");
   }

   QPair<AccessToken, RefreshToken> OAuthProxyFlow::parseTokensFromJsonResponse(const QJsonDocument & jsonResponse) const
   {
      QPair<AccessToken, RefreshToken> tokens;

      if (!jsonResponse.isObject())
      {
         qDebug() << "qs3i::AuthenitcationManager::getAccessToken -> Response body was not a json object";
         return tokens;
      }
      const auto replyObject = jsonResponse.object();

      QString accessTokenString = jsonResponse["access_token"].toString();
      if (accessTokenString.isNull() || accessTokenString.isEmpty())
      {
         qDebug() << "qs3i::AuthenitcationManager::getAccessToken -> Received Access Token was not a string";
         return tokens;
      }

      const auto accessToken = AccessToken(accessTokenString);

      if (accessToken.isEmpty())
      {
         qDebug() << "qs3i::AuthenitcationManager::getAccessToken -> Could not parse AccessToken";
         return tokens;
      }

      tokens.first = accessToken;

      QString refreshTokenString = jsonResponse["refresh_token"].toString();
      if (refreshTokenString.isNull() || refreshTokenString.isEmpty())
      {
         qDebug() << "qs3i::AuthenitcationManager::getAccessToken -> Received RefreshToken was not a string";
         return tokens;
      }

      const auto refreshToken = RefreshToken(refreshTokenString);

      if (refreshToken.isEmpty())
      {
         qDebug() << "qs3i::AuthenitcationManager::getAccessToken -> Could not parse RefreshToken";
         return tokens;
      }

      tokens.second = refreshToken;

      return tokens;
   }
}