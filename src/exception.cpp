#include "exception.h"

#include <QString>

namespace S3I
{
   Exception::Exception(const QString& message)
      : std::exception()
      , m_msg(message)
   {
   }

   QString Exception::longWhat() const noexcept
   {
      return m_msg;
   }


   NetworkException::NetworkException(QNetworkReply* reply)
      : Exception(reply->errorString())
      , error(reply->error())
      , errorString(reply->errorString().toLocal8Bit())
      , serverResponse(reply->readAll())
      , request(reply->request())
   {
   }

   QString NetworkException::longWhat() const noexcept
   {
      return  m_msg + " | Reply body: '" + serverResponse + "'";
   }

   const char* NetworkException::what() const
   {
       return errorString.constData();
   }
   
   ParseException::ParseException(const QString & message)
      : Exception(message)
   {
   }

   InvalidJsonException::InvalidJsonException(const QString & message)
      : ParseException(message)
   {
   }

   SchemaException::SchemaException(const QString & message)
      : ParseException(message)
   {
   }

   JWTParseException::JWTParseException(const QString & message)
      : ParseException(message)
   {
   }

   MaxRetryException::MaxRetryException(const QString & message)
      : Exception(message)
   {
   }

   KeyException::KeyException(const QString & message)
      : Exception(message)
   {
   }

   
}